/* eslint-disable import/no-extraneous-dependencies */
import {configure,addParameters} from '@storybook/vue'
import '../../src/main.requirements.js'

//import Highcharts from 'highcharts'
//import wordcloudInit from 'highcharts/modules/wordcloud'

//wordcloudInit(Highcharts)


const req = require.context('../../src/components', true, /.stories.js$/)

function loadStories() {
  req.keys().forEach(filename => req(filename))
}

addParameters({ options: { showAddonPanel: false } });

configure(loadStories, module)
