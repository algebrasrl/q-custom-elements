const path = require('path')

module.exports = {
  lintOnSave: true,

  // mine 1
  chainWebpack: config => {
    var _ext = {
//      'vue': 'Vue',
//      'axios': 'axios',

//      'vue-custom-element': 'VueCustomElement',

//      'swiper': 'Swiper',
//      'moment': 'moment',

//       'markerclusterer': 'MarkerClusterer',
//       'overlappingmarkerspiderfier': 'OverlappingMarkerSpiderfier',

//      'highcharts': 'Highcharts',
//      'highcharts/modules/wordcloud': 'wordcloudInit',  // in module
//      'highcharts-vue': 'HighchartsVue'
    }
    if (process.env.NODE_ENV === 'production') {
//      _ext['vue'] = 'Vue'
    }
    config.externals(_ext)

    // Se è necessario tenere tutto il codice in due file (js e css)
    // https://forum.vuejs.org/t/disable-code-splitting-in-vue-cli-3/36295/3
    //  config.optimization.delete('splitChunks')
    //  config.optimization.splitChunks(false)

    // Abilitazione Markdown in storybook
    config.module.rule('md')
      .test(/\.md/)
      .use('vue-loader')
      .loader('raw-loader')
      .end()

  },

  // https://stackoverflow.com/questions/58184549/sass-loader-error-invalid-options-object-that-does-not-match-the-api-schema
  css: {
    loaderOptions: {
        sass: {
            sassOptions: {
                includePaths: ['./node_modules'] //,
                // indentedSyntax: true
            }
        }
    }
  },

  publicPath: process.env.NODE_ENV === 'production'
    ? '/'
    : '/',
  assetsDir: 'static',

  transpileDependencies: [
    "@paiuolo/pai-vue-google-maps",
    "@paiuolo/pai-vue-mixins",
    "@paiuolo/pai-vue-swiper",
    "@paiuolo/pai-vue-gettext",

    "@paiuolo/django-sso-vue-mixins",
    "@paiuolo/django-esb-vue-mixins",
    "@paiuolo/django-uploads-vue-mixins",
    "@paiuolo/elk-vue-mixins",
    
    "browser-md5-file/dist",

    "@material",
  ]
}
