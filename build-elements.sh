#!/bin/sh

set -e

VERSION=0.3.3


# elements
./build-custom-element.sh friendly-iframe $VERSION y


# entities

./build-custom-element.sh user-top-bar-avatar $VERSION y

./build-custom-element.sh loyalty-user-profile $VERSION y
./build-custom-element.sh loyalty-campaign-list $VERSION y
./build-custom-element.sh loyalty-reward-list $VERSION y
./build-custom-element.sh loyalty-contact-form $VERSION y

./build-custom-element.sh rumor-level $VERSION y
./build-custom-element.sh rumor-level-timeline $VERSION y
./build-custom-element.sh rumor-word-cloud $VERSION y
./build-custom-element.sh rumor-list $VERSION y

./build-custom-element.sh rumor-dashboard $VERSION y
#./build-custom-element.sh rumor-dashboard-list $VERSION y

#./build-custom-element.sh rumor-level-trend 0.0.1 y
npx vue-cli-service build --inline-vue --dest "./dist/rumor-level-trend/0.0.1" --target lib --name rumor-level-trend ./src/rumor-level-trend.main.js
#./build-custom-element.sh rumor-level-histogram 0.0.1 y
npx vue-cli-service build --inline-vue --dest "./dist/rumor-level-histogram/0.0.1" --target lib --name rumor-level-histogram ./src/rumor-level-histogram.main.js
