#!/bin/sh

set -e

P_NAME=$1
P_VER=$2
SKIP=$3

if [ -z "$P_NAME" ]
  then
    echo "No package name specified (i.e. example-element)"
    exit 1
fi

if [ -z "$P_VER" ]
  then
    echo "No package version specified (i.e. 0.0.1)"
    exit 1
fi

MAIN_JS="src/$1.main.js"

if [ -f $MAIN_JS ]
  then
    echo "Main JS file: $MAIN_JS"
fi

if [ -z $SKIP ]
  then
    read -p "Press 'y' to continue " CONTINUE

    if [ "$CONTINUE" = "y" ]
      then
        echo "Checking main file ($MAIN_JS)"
      else
        exit 0
    fi
  else
    echo ".."
fi

if [ -f $MAIN_JS ]
  then
    echo "$MAIN_JS found, start building.."
  else
    echo "$MAIN_JS not found."
    exit 1
fi


npx vue-cli-service build --dest "./dist/$P_NAME/$P_VER" --target lib --name "$P_NAME" "$MAIN_JS"
