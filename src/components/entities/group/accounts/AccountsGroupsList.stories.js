/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue'

import AccountsGroupsList from './AccountsGroupsList'
import notes from './AccountsGroupsList.md'


const template = `
<div class="contenitore">
    <AccountsGroupsList
        :groups="accountsGroups.results" />
</div>
`

storiesOf('accounts', module)
  .add('AccountsGroupsList', () => ({
    components: { AccountsGroupsList },
    template: template,
    data () {
      return {
        accountsGroups: {"count":2,"next":null,"previous":null,"results":[{"url":"https://accounts.q-cumber.org/api/v1/auth/groups/1/","id":1,"name":"staff"},{"url":"https://accounts.q-cumber.org/api/v1/auth/groups/2/","id":2,"name":"pescatore"}]},
      }
    }
  }), {
    notes
  })
