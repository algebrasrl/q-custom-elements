/**
 * @mixin
 * Defines the base common interface to interact with
 * ig-wordcloud es serverless-functions.
 */

import ServelessFunctionCallMixin from '../../../mixins/ServelessFunctionCallMixin'


export default {
  mixins: [
    ServelessFunctionCallMixin
  ],
  props: {
    showExcluded: {
      type: Boolean,
      default: false
    },
    sfUrl: {
      type: String,
      default: 'https://data.q-cumber.org/serverless-functions/ig-wordcloud/'
    },
    size: {
       type: [String, Number],
       default: 100
    },
    subject: {
       type: String,
       default: null
    },
    topic: {
      type: String,
      default: null
    },
    basins: {
      type: String,
      default: null
    },
    excludedWords: {
      type: String,
      default: null
    },
    normalized: {
      type: Boolean,
      default: false
    },
    isNegative: {
      type: Boolean,
      default: true
    }
  },
  data () {
    return {
      excluded: this.excludedWords
    }
  },
  methods: {
    parseEntities (data) {
      //console.log('data', data)

      let e = data.aggregations.most_used_terms.buckets.map((e) => {
        return {
          name: e['key'],
          weight: e['doc_count']
        }
      })

      return e
    },
    getEntities () {
      //console.log('getEntities')
      this.apiError = false
      this.notAuthorized = false

      this.xhrGetUrl = this.sfUrl

      if (this.size) this.xhrGetUrl += '?size=' + parseInt(this.size)
      if (this.useTimezone) this.xhrGetUrl += '&timezone=' + this.timezone
      if (this.startDate) this.xhrGetUrl += '&from=' + this.normalizeDate(this.startDate)
      if (this.endDate) this.xhrGetUrl += '&to=' + this.normalizeDate(this.endDate)
      if (this.subject) this.xhrGetUrl += '&subject=' + this.subject
      if (this.topic) this.xhrGetUrl += '&topic=' + this.topic
      if (this.basins) this.xhrGetUrl += '&basins=' + this.basins
      
      if (this.excludedWords){
        let exclude = this.excludedWords.toLowerCase().replace(/ /g, '|').replace(/_/g, '|')
        this.xhrGetUrl += '&exclude=' + exclude
      }

      this.performXhrGet()
    },
    getLevelColor (x) {
      // console.log('COLOR', x, this.state.normalized)
      let color
      if (this.normalized) {
        let a = this.isNegative ? (1-x) : x
        let hue = a * 120
        color = "hsl(" + hue + ", 90%, 50%)"
      }
      // console.log('FOTERTR,', color)
      return color
    }
  }
}
