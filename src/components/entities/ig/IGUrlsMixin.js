/**
 * @mixin
 * Defines the base common interface to interact with
 * ig-urls es serverless-functions.
 */
import moment from 'moment'

import ServelessFunctionCallMixin from '../../../mixins/ServelessFunctionCallMixin'


export default {
  mixins: [
    ServelessFunctionCallMixin
  ],
  props: {
    sfUrl: {
      type: String,
      default: 'https://data.q-cumber.org/serverless-functions/ig-urls/'
    },
    size: {
      type: [String, Number],
      default: 10
    },
    start: {
      type: [String, Number],
      default: 0
    },
    subject: {
       type: String,
       default: null
    },
    topic: {
      type: String,
      default: null
    },
    basins: {
      type: String,
      default: null
    }
  },
  data () {
    return {
      loadFrom: parseInt(this.start)
    }
  },
  mounted () {
    this.$on('userIsAnonymous', ()=>{
      this.stopLoading()
    })
  },
  computed: {},
  methods: {
    parseEntities (data) {
      // console.log('data', data)
      this.maxFound = data.hits.total.value

      let e = data.hits.hits.map((e) => {
        return e['_source']
      })

      if (this.loadFrom > 0) {
        e = this.entities.concat(e)
      }

      return e
    },
    getEntities () {
      // console.log('getEntities')
      this.apiError = false
      this.notAuthorized = false

      let action = 'search'
      this.xhrGetUrl = this.sfUrl + '?action=' + action

      if (this.loadFrom) this.xhrGetUrl += '&start=' + this.loadFrom
      if (this.size) this.xhrGetUrl += '&size=' + parseInt(this.size)
      if (this.useTimezone) this.xhrGetUrl += '&timezone=' + this.timezone
      if (this.startDate) this.xhrGetUrl += '&from=' + this.normalizeDate(this.startDate)
      if (this.endDate) this.xhrGetUrl += '&to=' + this.normalizeDate(this.endDate)
      if (this.subject) this.xhrGetUrl += '&subject=' + this.subject
      if (this.topic) this.xhrGetUrl += '&topic=' + this.topic
      if (this.basins) this.xhrGetUrl += '&basins=' + this.basins

      this.performXhrGet()
    },
    loadMore () {
      this.loadFrom += parseInt(this.size)
      this.getEntities()
    },
    hasMore () {
      let asksFor = this.loadFrom + parseInt(this.size)
      let shoudHave = this.loadFrom
      let has = this.entities.length
      let hasMore = this.maxFound > 0 && asksFor < this.maxFound && (has - shoudHave) >= 0
      //console.log(this.loadFrom, 'asksFor', asksFor, 'shoudHave', shoudHave, 'has', has, 'hasMore', hasMore)
      return hasMore
    },
    getEntityDate (e) {
      // console.log('getEntityDate', e.doc, e.doc.meta.created_at)
      let date = new Date(e.doc.meta.created_at * 1000)
      return moment(date).calendar()
    },
    getEntityTitle (e) {
      if (e) {
        // console.log('entity', e, e.doc.meta.scraping.basin)
        if (e.doc.meta.scraping.basin == 'twitter') {
          return e.doc.meta.twitter.full_text
        } else if (e.doc.meta.scraping.basin == 'rss') {
          return e.doc.meta.rss.title
        } else if (e.doc.meta.scraping.basin == 'google-cse') {
          return e.doc.meta.lead
        } else {
          return e.doc.body
        }
      }
    },
    getEntityUrl (e) {
      return e.doc.meta.url
    }
  }
}
