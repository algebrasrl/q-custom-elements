/**
 * @mixin
 * Defines the base common interface to interact with
 * ig-subjects es serverless-functions.
 */
import moment from 'moment'

import ServelessFunctionCallMixin from '../../../mixins/ServelessFunctionCallMixin'


export default {
  mixins: [
    ServelessFunctionCallMixin
  ],
  props: {
    sfUrl: {
      type: String,
      default: 'https://data.q-cumber.org/serverless-functions/ig-subjects/'
    },
    size: {
      type: [String, Number],
      default: 10000
    },
    start: {
      type: [String, Number],
      default: 0
    },
    group: {
       type: String,
       default: 'comune'
    },
    topic: {
      type: String,
      default: null
    },
    timezone: {
      type: String,
      default: null
    }
  },
  data () {
    return {
      loadFrom: parseInt(this.start)
    }
  },
  mounted () {
    this.$on('userIsAnonymous', ()=>{
      this.stopLoading()
    })
  },
  computed: {},
  methods: {
    parseEntities (data) {
      // console.log('data', data)

      let e = data.aggregations.subjects.buckets.map((e) => {
        return {
          'subject': e.key,
          'alias': e.alias.buckets[0].key,
          'doc_count': e.doc_count,
        }
      })

      return e
    },
    getEntities () {
      // console.log('getEntities')
      this.apiError = false
      this.notAuthorized = false

      let action = 'search'
      this.xhrGetUrl = this.sfUrl + '?group=' + this.group

      if (this.topic) this.xhrGetUrl += '&topic=' + this.topic
      if (this.loadFrom) this.xhrGetUrl += '&start=' + this.loadFrom
      if (this.size) this.xhrGetUrl += '&size=' + parseInt(this.size)
      if (this.timezone) this.xhrGetUrl += '&timezone=' + this.timezone
      if (this.startDate) this.xhrGetUrl += '&from=' + this.normalizeDate(this.startDate)
      if (this.endDate) this.xhrGetUrl += '&to=' + this.normalizeDate(this.endDate)

      this.performXhrGet()
    }
  }
}
