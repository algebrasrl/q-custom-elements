/**
 * @mixin
 * Defines the base common interface to interact with
 * ig-timeline es serverless-functions.
 */
import moment from 'moment'

import ServelessFunctionCallMixin from '../../../mixins/ServelessFunctionCallMixin'

import IGAreasMixin from './IGAreasMixin'

export default {
  mixins: [
    ServelessFunctionCallMixin,
    IGAreasMixin
  ],
  props: {
    sfUrl: {
      type: String,
      default: 'https://data.q-cumber.org/serverless-functions/ig-timeline/'
    },
    interval: {
      type: String,
      default: 'week'
    },
    steps: {
      type: Number,
      default: 13
    },
    subject: {
       type: String,
       default: null
    },
    topic: {
      type: String,
      default: null
    }
  },
  data () {
    return {
      startDate: this.from ? moment(this.from).toDate() : moment().startOf(this.interval).subtract(this.steps, this.interval).toDate(),
      endDate: this.to ? moment(this.to).toDate() : moment().startOf(this.interval).toDate(),
      shift: 0
    }
  },
  mounted () {
    //console.log('startDate', this.startDate, 'endDate', this.endDate)
  },
  methods: {
    parseEntities (data) {
      // console.log('data', data)
      let e = data.aggregations.by_interval.buckets
      return e
    },
    getEntities () {
      // console.log('getEntities')
      this.xhrGetUrl = this.sfUrl

      this.xhrGetUrl += '?interval=' + this.interval
      if (this.timezone) this.xhrGetUrl += '&timezone=' + this.timezone
      if (this.startDate) this.xhrGetUrl += '&from=' + this.normalizeDate(this.startDate)
      if (this.endDate) this.xhrGetUrl += '&to=' + this.normalizeDate(this.endDate)
      if (this.subject) this.xhrGetUrl += '&subject=' + this.subject
      if (this.topic) this.xhrGetUrl += '&topic=' + this.topic

      this.performXhrGet()
    },
    getChartData () {
      let chartData = {};
      for (let i=0; i<this.areas.length; i++) {
        let area = this.areas[i]
        let basin = area.basins[0]
        //console.log('area', area.slug, 'basin', basin)
        let data = []
        for (let j=0; j<this.entities.length; j++) {
          let step = this.entities[j]
          //console.log('step', step)
          let stepBasins = step.basins.buckets.map((x) => {
            return x.key
          })
          if (stepBasins.filter((x) => {
            return x == basin
          }).length) {
            //console.log('basin found', basin)
            data.push(step.basins.buckets.find((x) => {
              return x.key == basin
            }).user.unique_url_count.value)
          } else {
            //console.log('basin NOT found', basin)
            data.push(0)
          }
        }
        chartData[basin] = data
      }
      return chartData
    },
    getState () {
      return {
        shift: this.shift,
        startDate: this.startDate,
        endDate: this.endDate,
        data: this.getChartData()
      }
    }
  },
  watch: {
    shift (x, y) {
      let n = y - x
      if (n < 0) {  // right
        let end_date = moment(this.endDate).add(1, this.interval + 's')
        let start_date = moment(end_date).subtract(this.steps, this.interval + 's')
        this.startDate = start_date.toDate()
        this.endDate = end_date.toDate()
      } else {  // left
        let end_date = moment(this.endDate).subtract(1, this.interval + 's')
        let start_date = moment(end_date).subtract(this.steps, this.interval + 's')
        this.startDate = start_date.toDate()
        this.endDate = end_date.toDate()
      }
      //console.log('date interval changed', x, y, this.startDate, this.endDate)
      this.getEntities()
      this.$emit('updated', this.getState())
    }
  }
}
