/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue'

import IGWordCloud from './IGWordCloud'
import notes from './IGWordCloud.md'


const template = `
<div class="contenitore" style="background-color: #fefefe; position: relative; height: 500px;">
  <IGWordCloud auto
    :excluded-words="excludedWords"
  />
</div>
`

const excludedWords = 'inquinamento|inquinanti'

storiesOf('ig', module)
  .add('IGWordCloud', () => ({
    components: { IGWordCloud },
    template: template,
    data () {
      return {
        excludedWords: excludedWords
      }
    },
    methods: {
    }
  }), {
    notes
  })
