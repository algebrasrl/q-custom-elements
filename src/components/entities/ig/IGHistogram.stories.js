/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue'

import IGHistogram from './IGHistogram'
//import notes from './IGHistogram.md'

import Multiselect from 'vue-multiselect'

import IGSubjectsMixin from './IGSubjectsMixin'

const template = `
<div style="padding: 12px;">
    <IGHistogram
      title="inquinamento / web"
      basin="google-cse"
      topic="inquinamento" />

    <IGHistogram
      title="inquinamento / social"
      basin="twitter"
      topic="inquinamento" />

    <IGHistogram
      title="inquinamento / feed"
      basin="rss"
      topic="inquinamento" />

</div>
`

storiesOf('IG', module)
  .add('IGHistogram', () => ({
    mixins: [
      IGSubjectsMixin
    ],
    components: {
      Multiselect,
      IGHistogram
   },
   data () {
    return {
      subjectsComune: [],
      selectedSubjectsComune: [],
      isLoading: false,
      basins: [
        {
          slug: 'google-cse',
          name: 'Google CSE'
        },
        {
          slug: 'twitter',
          name: 'Twitter'
        },
        {
          slug: 'rss',
          name: 'RSS'
        },
      ],
      selectedTopic: 'inquinanti',
      loadingTopic: false
    }
   },
   mounted () {},
   methods: {
      limitText (count) {
        return `and ${count} other subjects`
      },
      clearComune () {
        this.selectedSubjectsComune = []
      }
    },
    watch: {
        selectedTopic: function (n, o) {
          this.loadingTopic = true
          setTimeout(() => {
            this.loadingTopic = false
          }, 1000)
        }
    },
    template: template
  }), {
    //notes
  })
