/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue'

import IGUrls from './IGUrls'
//import notes from './RumorList.md'


const template = `
<div class="contenitore" style="background-color: #fefefe;">
    <IGUrls
      topic="inquinamento"
      auto />
</div>
`

storiesOf('IG', module)
  .add('IGUrls', () => ({
    components: { IGUrls },
    template: template
  }))
  /*, {
    notes
  })*/
