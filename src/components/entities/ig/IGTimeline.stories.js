/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue'

import IGTimeline from './IGTimeline'
import notes from './IGTimeline.md'


const template = `
<div class="contenitore" style="background-color: #fefefe;">
    <IGTimeline
        auto
        subject="024f584d-f5b9-41e2-9c08-a376ace95117"
        topic="inquinamento"
        title="Regione Lombardia / Inquinamento"
        interval="week"
        />
</div>
`

storiesOf('ig', module)
  .add('IGTimeline', () => ({
    components: { IGTimeline },
    template: template,
    data () {
      return {}
    }
  }), {
    notes
  })
