/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue'

import IGTrend from './IGTrend'
//import notes from './IGTrend.md'


const template = `
<div class="contenitore" style="background-color: #fefefe;">
  <div>
    <IGTrend trend="1,2,3,3,4" />
  </div>
</div>
`

storiesOf('IG', module)
  .add('IGTrend', () => ({
    components: { IGTrend },
    template: template
  }), {
    //notes
  })
