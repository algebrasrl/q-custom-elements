/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue'
import { action } from '@storybook/addon-actions'

import IGDashboard from './IGDashboard'
//import notes from './IGDashboard.md'

import Multiselect from 'vue-multiselect'

import IGSubjectsMixin from './IGSubjectsMixin'

// howto
//<iframe src="https://storybook.q-cumber.org:8080/iframe.html?id=ig--igdashboard" style="overflow:hidden;height:100%;width:100%; min-height:640px;" width="100%" height="100%" frameborder="0"></iframe>


const template = `
<div style="padding: 12px;">

<div style="padding: 4px; margin-bottom: 8px;">
<select style="font-size: 19px; padding: 10px;"
  v-model="selectedTopic">
  <option disabled value="">Topic</option>
  <option>inquinanti</option>
  <option>inquinamento</option>
</select>
</div>

<div style="padding: 4px;" v-if="!loadingTopic">
    <div style="margin-bottom: 20px;">
      <multiselect v-model="selectedSubjectsComune"
        id="ajax"
        label="alias"
        track-by="subject"
        :placeholder="isLoading ? 'Loading..' : 'Soggetti'"
        open-direction="bottom"
        :options="entities"
        :multiple="true"
        :searchable="!isLoading"
        :loading="isLoading"
        :clear-on-select="false"
        :close-on-select="true"
        :options-limit="300"
        :limit="5"
        :limit-text="limitText"
        :max-height="240"
        :show-no-results="false"
        :hide-selected="true">
        <template slot="clear" slot-scope="props">
          <div class="multiselect__clear" v-if="entities.length" @mousedown.prevent.stop="clearComune(props.search)"></div>
        </template><span slot="noResult">Oops! No elements found. Consider changing the search query.</span>
      </multiselect>
      <!--<pre class="language-json"><code>{{ selectedSubjectsComune }}</code></pre>-->
    </div>

    <div v-for="(item, index) in selectedSubjectsComune" v-bind:key="index">
      <hr/>

      <IGDashboard
        hide-login-mask
        :subject="item.subject"
        :subject-name="item.alias"
        :topic="selectedTopic"
        :topic-name="selectedTopic"/>

        <br/>

    </div>
  </div>
</div>
`

storiesOf('IG', module)
  .add('IGDashboard', () => ({
    mixins: [
      IGSubjectsMixin
    ],
    components: {
      Multiselect,
      IGDashboard
   },
   data () {
    return {
      selectedSubjectsComune: [],
      selectedTopic: 'inquinanti',
      loadingTopic: false,
      isLoading: false
    }
   },
   mounted () {},
   methods: {
      limitText (count) {
        return `and ${count} other subjects`
      },
      clearComune () {
        this.selectedSubjectsComune = []
      }
    },
    watch: {
      selectedTopic: function (n, o) {
        this.loadingTopic = true
        setTimeout(() => {
          this.loadingTopic = false
        }, 200)
      }
    },
    template: template
  }), {
    //notes
  })
