/**
 * @mixin
 * IG Areas base properties
 */


export default {
  data () {
    return {
      selectedAreas: null,
      areas: [
        {
          slug: 'web',
          name: 'Web',
          basins: ['google-cse'],
          color: 'lightcoral'
        },
        {
          slug: 'social',
          name: 'Social',
          basins: ['twitter'],
          color: 'lightblue'
        },
        {
          slug: 'feed',
          name: 'Feed',
          basins: ['rss'],
          color: 'lightgreen'
        }
      ]
    }
  },
  mounted () {
    if (!this.selectedAreas) {
      this.selectedAreas = this.areas.map((x)=>{ return x.slug })
    }
  }
}
