/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue'

import QpostCounter from './QpostCounter'
import notes from './QpostCounter.md'


const template = `
<div class="contenitore" style="background-color: #fefefe;">
    <QpostCounter
        qpost_grigio="10"
        qpost_rosso="4"
        qpost_giallo="2"
        qpost_verde="3"
        />
</div>
`

storiesOf('qpost', module)
  .add('QpostCounter', () => ({
    components: { QpostCounter },
    template: template
  }), {
    notes
  })
