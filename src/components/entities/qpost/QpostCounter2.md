QpostCounter2

## Examples

### Default

```jsx
  <QpostCounter2
    qpost_verde="101"
    qpost_giallo="3"
    qpost_rosso="21"
    qpost_grigio="5"
    qpost_rosso_ticket="1"
   >
  </QpostCounter2>
```

### External

```jsx
  <QpostCounter2
    auto
   >
  </QpostCounter2>
```
