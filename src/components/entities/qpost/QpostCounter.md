QpostCounter

## Examples

Default:

```jsx
<QpostCounter
  qpost_grigio="10"
  qpost_rosso="4"
  qpost_giallo="2"
  qpost_verde="3"
  />
```

### Remote

```jsx
  <QpostCounter
    auto
   >
  </QpostCounter>
```
