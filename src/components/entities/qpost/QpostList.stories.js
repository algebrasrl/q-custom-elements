/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue'

import QpostList from './QpostList'
import notes from './QpostList.md'


const template = `
<div class="contenitore" style="background-color: #fefefe;">
    <QpostList
      auto/>
</div>
`

storiesOf('qpost', module)
  .add('QpostList', () => ({
    components: { QpostList },
    template: template
  }), {
    notes
  })
