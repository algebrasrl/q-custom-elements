/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue'

import QpostCounter2 from './QpostCounter2'
import notes from './QpostCounter2.md'


const template = `
<div class="contenitore" style="background-color: #fefefe;">
    <QpostCounter2
        qpost_verde="101"
        qpost_giallo="3"
        qpost_rosso="21"
        qpost_grigio="5"
        qpost_rosso_ticket="1">
    </QpostCounter2>
</div>
`

storiesOf('qpost', module)
  .add('QpostCounter2', () => ({
    components: { QpostCounter2 },
    template: template
  }), {
    notes
  })
