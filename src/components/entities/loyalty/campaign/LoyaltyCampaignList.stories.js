/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue'
import { action } from '@storybook/addon-actions'

import LoyaltyCampaignList from './LoyaltyCampaignList'
import notes from './LoyaltyCampaignList.md'


const template = `
<div class="contenitore loyalty">
    <LoyaltyCampaignList
        auto
        @userIsAnonymous="userIsAnonymous"
        @fetchUserProfileSuccess="fetchUserProfileSuccess"
        />
</div>
` 

storiesOf('loyalty', module)
  .add('LoyaltyCampaignList', () => ({
    components: { LoyaltyCampaignList },
    template: template,
    methods: {
      userIsAnonymous: action('userIsAnonymous'),
      fetchUserProfileSuccess: action('fetchUserProfileSuccess')
    }
  }), {
    notes
  })
