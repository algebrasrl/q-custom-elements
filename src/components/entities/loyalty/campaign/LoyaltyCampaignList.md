LoyaltyCampaignList

## Examples

Default:

```jsx
<div style="background-color: #333; padding: 16px;">
  <LoyaltyCampaignList
    :subscriber="globalUserProfile"
  ></LoyaltyCampaignList>
</div>
```
