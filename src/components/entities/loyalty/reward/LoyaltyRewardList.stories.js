/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue'
import { action } from '@storybook/addon-actions'

import LoyaltyRewardList from './LoyaltyRewardList'
import notes from './LoyaltyRewardList.md'


const template = `
<div class="contenitore loyalty">
    <LoyaltyRewardList />
</div>
` 

storiesOf('loyalty', module)
  .add('LoyaltyRewardList', () => ({
    components: { LoyaltyRewardList },
    template: template,
    methods: {
      click: action('click')
    }
  }), {
    notes
  })
