/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue'
import { action } from '@storybook/addon-actions'

import LoyaltyUserProfile from './LoyaltyUserProfile'
import notes from './LoyaltyUserProfile.md'


const template = `
<div class="contenitore loyalty">
    <LoyaltyUserProfile
      auto
      @userIsAnonymous="userIsAnonymous"
      @fetchUserProfileSuccess="fetchUserProfileSuccess"
      />
</div>
` 

storiesOf('loyalty', module)
  .add('LoyaltyUserProfile', () => ({
    components: { LoyaltyUserProfile },
    template: template,
    methods: {
      userIsAnonymous: action('userIsAnonymous'),
      fetchUserProfileSuccess: action('fetchUserProfileSuccess')
    }
  }), {
    notes
  })
