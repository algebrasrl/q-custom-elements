LoyaltyUserProfile

## Examples

Default:

```jsx
<div style="background-color: #333; padding: 16px;">
  <LoyaltyUserProfile
    :external-user-profile="globalUserProfile"
  ></LoyaltyUserProfile>
</div>
```
