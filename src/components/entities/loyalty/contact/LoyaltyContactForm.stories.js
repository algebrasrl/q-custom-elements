/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue'

import LoyaltyContactForm from './LoyaltyContactForm'
import notes from './LoyaltyContactForm.md'


const template = `
<div class="contenitore loyalty">
    <LoyaltyContactForm />
</div>
`

storiesOf('loyalty', module)
  .add('LoyaltyContactForm', () => ({
    components: { LoyaltyContactForm },
    template: template
  }), {
    notes
  })
