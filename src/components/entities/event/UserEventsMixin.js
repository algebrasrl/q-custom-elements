/**
 * @mixin
 */

import DjangoSsoAppUserElementMixin from '@paiuolo/django-sso-app-vue-mixins/mixins/DjangoSsoAppUserElementMixin'

import EntityListMixin from '@paiuolo/pai-vue-mixins/mixins/EntityListMixin'
import xhrGetMixin from '@paiuolo/pai-vue-mixins/mixins/xhrGetMixin'


export default {
  mixins: [
    EntityListMixin,
    xhrGetMixin,
    DjangoSsoAppUserElementMixin
  ],
  props: {
    auto: {
      type: Boolean,
      default: false
    },
    userProfileUrl: {
      type: String,
      default: 'https://accounts.q-cumber.org/api/v1/auth/profile/'
    },
    loginUrl: {
      type: String,
      default: 'https://accounts.q-cumber.org/login/'
    },
    eventTypes: {
      type: String,
      default: null
    },
    sfUrl: {
      type: String,
      default: 'https://events.q-cumber.org/serverless-functions/get-user-events/'
    },
    sfAction: {
      type: String,
      default: 'get'
    },
    size: {
      type: [String, Number],
      default: 10
    },
    from: {
      type: [String, Number],
      default: 0
    },
    startDate: {
      type: String,
      default: 'now-1y'
    },
    endDate: {
      type: String,
      default: 'now'
    },
    userId: {
      type: String,
      default: null
    }
  },
  data () {
    return {
      eventTypesD: this.eventTypes ? this.eventTypes.split(',').map((el)=>{return el.replace(/ /g, '')}) : [], // ['qpost_verde', 'qpost_giallo', 'qpost_grigio', 'qpost_rosso', 'qpost_rosso_ticket'],
      loadFrom: this.from,
      maxFound: 0,

      dStartDate: this.startDate,
      dEndDate: this.endDate,

      bbTopLeftLat: null,
      bbTopLeftLon: null,
      bbBottomRightLat: null,
      bbBottomRightLon: null,

      xhrGetParams: {
        withCredentials: true
      },
      xhrGetPermittedErrors: '502,403',
      xhrGetMaxRetries: 5,
      xhrGetRetryTimeout: 199,
      xhrGetRetryOnPermittedError: true,
      apiError: false,
      notAuthorized: false,

      fetchingEvents: false
    }
  },
  mounted () {
    this.$on('performingXhrGet', () => {
//       console.log('performingXhrGet')
      this.apiError = false

      this.fetchingEvents = true
      this.startLoading()

      this.$emit('performingGetEntities')
    })
    this.$on('xhrGetResponse', (res) => {
//       console.log('xhrGetResponse')
      this.entities = this.parseEntities(res.data)

      this.$emit('getEntitiesSuccess', res.data)

      this.fetchingEvents = false
      this.stopLoading()
    })
    this.$on('xhrGetError', (err) => {
//       console.log('xhrGetError', err)
      this.apiError = true

      this.$emit('getEntitiesError', err)

      this.fetchingEvents = false
      this.stopLoading()
    })

    if (this.auto) {
      this.getEntities()
    }
  },
  methods: {
    canGetEvents (obj) {
      return this.userProfile && !this.fetchingEvents
    },
    getBucketCount (B, s) {
      for (let i=0; i<B.length; i++) {
        let b = B[i]
        if (b.key === s) {
          return b.doc_count
        }
      }
      return 0
    },
    parseEntities (data) {
      this.maxFound = data.hits.total.value

      let e = data.hits.hits.map((e) => {
        return e['_source']['doc']
      })

      if (this.loadFrom > 0) {
        e = this.entities.concat(e)
      }

      return e
    },
    getEvents () {
      if (this.canGetEvents()) { 
        this.fetchingEvents = true

        this.xhrGetUrl = this.sfUrl
        this.xhrGetUrl += '?action=' + this.sfAction
        this.xhrGetUrl += '&event_types=' + this.eventTypesD.join(',')
        this.xhrGetUrl += '&size=' + this.size

        if (this.dStartDate) {
            this.xhrGetUrl += '&start_date=' + this.dStartDate
        }
        if (this.dEndDate) {
            this.xhrGetUrl += '&end_date=' + this.dEndDate
        }

        if (this.bbTopLeftLat && this.bbTopLeftLon && this.bbBottomRightLat && this.bbBottomRightLon) {
          this.xhrGetUrl += '&bb_top_left_lat=' + this.bbTopLeftLat + '&bb_top_left_lon=' + this.bbTopLeftLon
            + '&bb_bottom_right_lat=' + this.bbBottomRightLat + '&bb_bottom_right_lon=' + this.bbBottomRightLon
        }
        
        if (this.userId) this.xhrGetUrl += '&user_id=' + this.userId
        if (this.loadFrom > 0) this.xhrGetUrl += '&from=' + this.loadFrom

        console.log('getEntities', this.xhrGetUrl)

        this.performXhrGet()
      }
    },
    getEntities () {
      if (this.userProfile) {        
          this.getEvents()      
      } else {
        this.fetchUserProfile()
      }
    },
    loadMore () {
      this.loadFrom += parseInt(this.size)
      this.getEntities()
    }
  },
  watch: {
    userId (n, o) {
      console.log('userId', n, o)
      if (n && this.auto) {
        this.getEntities()
      }
    },
    userProfile (p, o) {
      console.log('userProfile', p, o)
      if (p && this.auto) {
        console.log('auto è true', this.auto)
        this.loadFrom = 0
        let lang = 'it'
        if (p.language && p.language != '') {
          lang = p.language
        } else {
            lang = window.navigator.userLanguage || window.navigator.language || 'it'
            lang = lang.split("-")[0]
        }
        this.setLocale(lang)

        this.getEntities()
      }
    },
    eventTypes (n, o) {
      console.log('eventTypes', n, o)
      if (n) {
        this.eventTypesD = n.split(',').map((el)=>{return el.replace(/ /g, '')})

        if (this.auto) {
          this.loadFrom = 0
          this.getEntities()
        }
      }
    }
  }
}
