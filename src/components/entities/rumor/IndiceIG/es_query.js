export default (subjectSsoId, subjectGroup, topic) => {
  return {
    "from": 0,
    "size": 0,
    "aggs": {
      "basins": {
        "terms": {
          "field": "doc.meta.django-fisherman.basin.slug"
        },
        "aggs": {
          "min_number_of_results": {
            "min": {
              "field": "doc.meta.google-cse.cse_searchInformation.totalResults"
            }
          },
          "max_number_of_results": {
            "max": {
              "field": "doc.meta.google-cse.cse_searchInformation.totalResults"
            }
          },
          "avg_number_of_results": {
            "avg": {
              "field": "doc.meta.google-cse.cse_searchInformation.totalResults"
            }
          },
          "unique_url_count": {
            "cardinality": {
              "field": "doc.meta.url"
            }
          },
          "user": {
            "filter": {
              "bool": {
                "filter": [
                  {
                    "term": {
                      "doc.meta.django-fisherman.bait.user_sso_id": subjectSsoId
                    }
                  }
                ]
              }
            },
            "aggs": {
              "min_number_of_results": {
                "min": {
                  "field": "doc.meta.google-cse.cse_searchInformation.totalResults"
                }
              },
              "max_number_of_results": {
                "max": {
                  "field": "doc.meta.google-cse.cse_searchInformation.totalResults"
                }
              },
              "avg_number_of_results": {
                "avg": {
                  "field": "doc.meta.google-cse.cse_searchInformation.totalResults"
                }
              },
              "unique_url_count": {
                "cardinality": {
                  "field": "doc.meta.url"
                }
              }
            }
          },
          "by_week": {
            "date_histogram": {
              "min_doc_count": 0,
              "interval":"week",
              "field":"doc.created_at",
              "time_zone":"+00:00"
            },
            "aggs": {
              "min_number_of_results": {
                "min": {
                  "field": "doc.meta.google-cse.cse_searchInformation.totalResults"
                }
              },
              "max_number_of_results": {
                "max": {
                  "field": "doc.meta.google-cse.cse_searchInformation.totalResults"
                }
              },
              "avg_number_of_results": {
                "avg": {
                  "field": "doc.meta.google-cse.cse_searchInformation.totalResults"
                }
              },
              "unique_url_count": {
                "cardinality": {
                  "field": "doc.meta.url"
                }
              },
              "user": {
                "filter": {
                  "bool": {
                    "filter": [
                      {
                        "term": {
                          "doc.meta.django-fisherman.bait.user_sso_id": subjectSsoId
                        }
                      }
                    ]
                  }
                },
                "aggs": {
                  "min_number_of_results": {
                    "min": {
                      "field": "doc.meta.google-cse.cse_searchInformation.totalResults"
                    }
                  },
                  "max_number_of_results": {
                    "max": {
                      "field": "doc.meta.google-cse.cse_searchInformation.totalResults"
                    }
                  },
                  "avg_number_of_results": {
                    "avg": {
                      "field": "doc.meta.google-cse.cse_searchInformation.totalResults"
                    }
                  },
                  "unique_url_count": {
                    "cardinality": {
                      "field": "doc.meta.url"
                    }
                  }
                }
              }
            }
          },
          "avg_unique_url_count": {
            "avg_bucket": {
              "buckets_path": "by_week>unique_url_count"
            }
          }
        }
      }
    },
    "query": {
      "bool": {
        "filter": [
          {
            "term": {
              "doc.meta.django-fisherman.bait.user.groups": subjectGroup
            }
          },
          {
            "terms": {
              "doc.type": [
                "fish"
              ]
            }
          },
          {
            "term": {
              "doc.meta.django-fisherman.bait.name": topic
            }
          }
        ],
        "must_not": [
          {
            "term": {
              "doc.meta.django-fisherman.bait.user_sso_id": {
                "value": topic
              }
            }
          }
          /*
          // esclusa la licenza Google (Italia)
          ,{
            "term": {
              "doc.meta.django-fisherman.license.name": {
                "value": "Google (Italia)"
              }
            }
          }*/
        ]
      }
    }
  }
}
/*
          {
            "range": {
              "doc.created_at": {
                "lte": "2020-06-21T21:59:59.999Z",
                "gte": "2020-05-16T22:00:00.000Z",
                "time_zone": "+00:00"
              }
            }
          }
*/
