RumorLevelHistogram

## Examples

Default:

```jsx
<RumorLevelHistogram
  auto
  topic="odore"
  subject="odore"
  subject-name="Odore"
  topic-name="Odore"
  selected-license="Google (Italia)"
  />
```
