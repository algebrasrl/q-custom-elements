/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue'
import { action } from '@storybook/addon-actions'

import RumorDashboard from './RumorDashboard'
import notes from './RumorDashboard.md'

import Multiselect from 'vue-multiselect'

import USERS_COMUNE from '../pescatore_users_comune.json'
import USERS_IMPRESA from '../pescatore_users_impresa.json'

const exclude = 'comune|provincia|regione|inquinamento|sostenibilita|inquinanti'

const template = `
<div style="padding: 12px;">
<label>Topic: </label>
<select v-model="selectedTopic">
  <option disabled value="">Topic</option>
  <option>odore</option>
  <option>inquinamento</option>
  <option>sostenibilita</option>
  <option>inquinanti</option>
</select>

<div style="padding: 12px;" v-if="!loadingTopic">
  <div style="float: left; margin: 0; padding: 0; width: 49%;">
    <div>
      <multiselect v-model="selectedSubjectsComune"
        id="ajax"
        label="username"
        track-by="username"
        :placeholder="isLoading ? 'Loading..' : 'Comuni'"
        open-direction="bottom"
        :options="subjectsComune"
        :multiple="true"
        :searchable="!isLoading"
        :loading="isLoading"
        :clear-on-select="false"
        :close-on-select="true"
        :options-limit="300"
        :limit="5"
        :limit-text="limitText"
        :max-height="240"
        :show-no-results="false"
        :hide-selected="true">
        <template slot="clear" slot-scope="props">
          <div class="multiselect__clear" v-if="selectedSubjectsComune.length" @mousedown.prevent.stop="clearComune(props.search)"></div>
        </template><span slot="noResult">Oops! No elements found. Consider changing the search query.</span>
      </multiselect>
      <!--<pre class="language-json"><code>{{ selectedSubjectsComune }}</code></pre>-->
    </div>

    <div v-for="(subject, index) in selectedSubjectsComune" v-bind:key="index">
      <RumorDashboard
        auto
        as-list
        is-negative
        hide-login-mask
        :subject="subject.sso_id"
        :subject-name="subject.username"
        :topic="selectedTopic"
        :exclude="getExcludedTerms(subject.username)" />
    </div>
  </div>

  <!-- Imprese -->

  <div style="float: left; margin: 0; padding: 0; width: 49%; margin-left: 2%;">
    <div>
      <multiselect v-model="selectedSubjectsImpresa"
        id="ajax"
        label="username"
        track-by="username"
        :placeholder="isLoading ? 'Loading..' : 'Imprese'"
        open-direction="bottom"
        :options="subjectsImpresa"
        :multiple="true"
        :searchable="!isLoading"
        :loading="isLoading"
        :clear-on-select="false"
        :close-on-select="true"
        :options-limit="300"
        :limit="5"
        :limit-text="limitText"
        :max-height="240"
        :show-no-results="false"
        :hide-selected="true">
        <template slot="clear" slot-scope="props">
          <div class="multiselect__clear" v-if="selectedSubjectsImpresa.length" @mousedown.prevent.stop="clearImpresa(props.search)"></div>
        </template><span slot="noResult">Oops! No elements found. Consider changing the search query.</span>
      </multiselect>
      <!--<pre class="language-json"><code>{{ selectedSubjectsImpresa }}</code></pre>-->
    </div>

    <div v-for="(subject, index) in selectedSubjectsImpresa" v-bind:key="index">
      <RumorDashboard
        auto
        as-list
        is-negative
        :subject="subject.sso_id"
        :subject-name="subject.username"
        :topic="selectedTopic"
        :exclude="getExcludedTerms(subject.username)" />
    </div>
  </div>
</div>
</div>
`

storiesOf('rumor', module)
  .add('RumorDashboard', () => ({
    components: {
      Multiselect,
      RumorDashboard
   },
   data () {
    return {
      exclude: exclude,
      subjectsComune: [],
      selectedSubjectsComune: [],
      subjectsImpresa: [],
      selectedSubjectsImpresa: [],
      selectedTopic: 'odore',
      loadingTopic: false,
      isLoading: false
    }
   },
   mounted () {
    this.subjectsComune = USERS_COMUNE.results
    this.subjectsImpresa = USERS_IMPRESA.results

    /*
    this.isLoading = true
    this.$http.get('https://pescatore.q-cumber.org/api/v1/auth/users/?limit=2000&groups__name=comune', {withCredentials: true}).then(
      response => {
        this.subjectsComune = response.data.results
        this.isLoading = false
      })

    this.isLoading = true
    this.$http.get('https://pescatore.q-cumber.org/api/v1/auth/users/?limit=2000&groups__name=impresa', {withCredentials: true}).then(
      response => {
        this.subjectsImpresa = response.data.results
        this.isLoading = false
      })
    */

   },
   methods: {
      limitText (count) {
        return `and ${count} other subjects`
      },
      clearComune () {
        this.selectedSubjectsComune = []
      },
      clearImpresa () {
        this.selectedSubjectsImpresa = []
      },
      getExcludedTerms(username) {
        return exclude + '|' + username.split('_').join('|').toLowerCase()
      }
    },
    watch: {
        selectedTopic: function (n, o) {
          this.loadingTopic = true
          setTimeout(() => {
            this.loadingTopic = false
          }, 1000)
        }
    },
    template: template
  }), {
    notes
  })
