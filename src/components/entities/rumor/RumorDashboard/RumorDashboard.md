RumorDashboard

## Examples

Default:

```jsx
<RumorDashboard
    is-negative
    topic="odore"
    subject="134a2bc0-7c8d-4f14-aa83-78a539cb59bd"
    subjectName="Comune di Agnosine"
  />
```

List:

```jsx
<RumorDashboard
    as-list
    is-negative
    topic="odore"
    subject="134a2bc0-7c8d-4f14-aa83-78a539cb59bd"
    subjectName="Comune di Agnosine"
  />
```
