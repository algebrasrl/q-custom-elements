import moment from 'moment'
import Highcharts from 'highcharts'

import RumorElementMixin from '../RumorElementMixin'
import UserRumorsMixin from "../UserRumorsMixin"


export default {
    name: 'RumorDashboard',
    mixins: [
        RumorElementMixin,
        UserRumorsMixin
    ],
    props: {
        userProfileUrl: {
            type: String,
            default: 'https://accounts.q-cumber.org/api/v1/auth/user/'
        },
        loginUrl: {
            type: String,
            default: 'https://accounts.q-cumber.org/login/'
        },
        sfUrl: {
            type: String,
            default: 'https://pescatore.q-cumber.org/serverless-functions/new-rumor-dashboards/'
        },
        selectedStepKey: {
            type: String,
            default: 'w'
        },
        numberOfSteps: {
            type: Number,
            default: 5
        },
        isNegative: {
            type: Boolean,
            default: true
        },
        hideLatestArticles: {
            type: Boolean,
            default: false
        },
        exclude: {
            type: String,
            default: ''
        },
        backgroundColor: {
            type: String,
            default: '#f2f2f2'
        }
    },
    data () {
        return {
            chartOptions: null,

            shift: 0,

            steps: [],
            selectedPoints: [],

            showTimeline: true,

            apiError: false,
            noResults: false,

            intervalChanged: true,

            maxNumberOfResults: 0,
            minNumberOfResults: 0,
            userMaxNumberOfResults: 0,
            userMinNumberOfResults: 0,

            intervalBuckets: null,
            wordList: [],

            infoOpened: false,
            showLatestArticles: false,

            trend: 0,
            excludedByTopic: {
                'odore': 'odore|puzza|esalazione|miasma|tanfo|olezzo|odoraccio',
                'inquinamento': 'inquinamento|sversamento',
                'sostenibilita': 'sostenibilita|sostenibilità|ambientale',
            }
        }
    },
    mounted () {
        this.init()
    },
    computed: {
        lastDate () {
            return moment().add(this.shift, this.selectedStepKey).endOf('d')
        },
        upperBound () {
            return moment(this.lastDate).endOf(this.selectedStepKey)
        },
        lowerBound () {
            return moment(this.upperBound).subtract(this.numberOfSteps, this.selectedStepKey).startOf('d')
        },
        dateFormat () {
            return this.state.asList ? 'L' : 'LLL'
        },
        selectedLicensesNames () {
            let seen = {}
            let licenses = this.selectedPoints.filter(function(item) {
                let key = item.license.key
                // console.log('parsin point item', item, key)
                return seen.hasOwnProperty(key) ? false : (seen[key] = true)
            }).map(function(item) {
                return item.license.key
            })
            // console.log('licenses', licenses.join(''))
            return licenses
        },
        lowerselectedDate () {
            let lo = this.selectedPoints.reduce((min, p) => p.step.from < min ? p.step.from : min, this.upperBound)
            return (lo != this.upperBound) ? lo : null
        },
        upperSelectedDate () {
            let up = this.selectedPoints.reduce((max, p) => p.step.to > max ? p.step.to : max, this.lowerBound)
            return (up != this.lowerBound) ? up : null
        },
        excludedTerms () {
            let subjectTitle = this.subjectTitle || this.subject
            let topicTitle = this.topicTitle || this.topic

            let exclude = subjectTitle.toLowerCase().replace(/\s/g, '|') + '|' + topicTitle.toLowerCase().replace(/\s/g, '|')

            if (this.exclude && this.exclude.length) {
                exclude +=  '|' + this.exclude
            }
            if (this.excludedByTopic[this.topic]) {
                exclude += '|' + this.excludedByTopic[this.topic]
            }
            return exclude
        },
        fullExcludedTerms () {
            return this.excludedTerms + '|a|e|i|o|u' +
                '|io|tu|egli|ella|esso|noi|voi|essi' +
                '|di|da|in|con|su|per|tra|fra|sopra|sotto' +
                '|del|degli|delle|della|sulla|alla|alle|sulla|sulle|nella|nelle' +
                '|lui|lei|loro|coloro|colui|colei|costoro' +
                '|à|è|ì|ò|é|ù' +
                '|chi|che|cosa|dove|come|quando|perchè|perché|giacchè|giacché|percio|perciò|ciò|benchè|benché|siccome|altrimenti|ovviamente|quale|quali' +
                '|mio|tuo|suo|nostro|vostro|loro' +
                '|se|sè|sé|non|ne|nè|né' +
                '|un|uno|una|il|lo|la|li|gli|le' +
                '|benvenuto|benvenuta|benvenuti' +
                '|pdf|svg|jpg|jpeg|xml|html|http|https|www|accounts|login|logout' +
                '|comune|comuni|comunale|provincia|province|provinciale|regione|regioni|regionale|stato|stati|statale|nazione|nazioni|nazionale' +
                '|by|cookie|cookies|policy' +
                '|miglior|migliore|peggior|peggiore|bello|bella|brutto|brutta' +
                '|essere|esser|avere|aver|ha|ho' +
                '|me|te',
                '|and|or|not'
        }
    },
    methods: {
        showMore () {
            // console.log('showMore', this.state.asList)
            this.state.asList = !this.state.asList
            this.parseEntities(this.entities)
        },
        init () {
            // console.log('init RumorDashboard')
            this.getEntities()
        },
        updateLicensesNames (data) {
          this.licensesNames = data['aggregations']['licenses']['buckets'].map((l)=>{
            return l['key']
          })
          this.selectedLicenseName = this.selectedLicense || null
        },
        update () {
            this.showLatestArticles = false
            this.selectedPoints = []
            this.updateSteps()
            this.updateChartCategories()
        },
        updateTrend () {
            this.trend = 0
            this.showLatestArticles = false

            if (this.selectedPoints && this.selectedPoints.length) { // L = selectedPoints, returns L[L.length] - L [L.length - 1]
                let selectedPoints = this.selectedPoints.sort((a, b) => { // order points per potition
                    return a.x - b.x
                })

                let pointB = selectedPoints[selectedPoints.length - 1]
                let pointA = (selectedPoints.length > 1) ? selectedPoints[selectedPoints.length - 2] : pointB

                let val1 = this.getBasinUserValue(this.selectedBasinSlug, pointA.bucket)
                let val2 = this.getBasinUserValue(this.selectedBasinSlug, pointB.bucket)

                this.trend = val2 - val1
            } else {
                let basinLicensesBuckets = this.getBasinLicensesBuckets(this.entities)
                let licensesSteps = []
                for (let licenseC = 0; licenseC < basinLicensesBuckets.length; licenseC++) {
                    let license = basinLicensesBuckets[licenseC]
                    let licenseSteps = this.calcLicenseSteps(license)
                    licensesSteps.push({
                        license: license,
                        steps: licenseSteps
                    })
                }
                let values = Array.apply(null, {length: 5}).map(_=>0);
                for (let i=0; i<licensesSteps.length; i++) {
                    let step = licensesSteps[i]
                    let steps = step.steps
                    for (let j=0; j<steps.length; j++) {
                        let licenseStepBucket = steps[j].bucket
                        let val = this.getBasinUserValue(this.selectedBasinSlug, licenseStepBucket)
                        values[j] += val
                    }
                }
                let val1 = 0
                let val2 = 0
                for (let i=0; i<values.length; i++) {
                    let val = values[i]
                    if (val) {
                        val1 = val2
                        val2 = val
                    }
                }
                this.trend = val2 - val1
            }
        },
        updateSteps () {
            // console.log('updateSteps', this.numberOfSteps)
            let L = []

            //console.log('LOWER', this.lowerBound.format('LLL'), 'UPPER', this.upperBound.format('LLL'))
            let lastStepEnd = moment(this.upperBound)
            let previousStepStart = moment(lastStepEnd).subtract(1, this.selectedStepKey).add(1, 'd').startOf('d')

            for (let i=1; i<=this.numberOfSteps; i++) {
                // console.log('DA', previousStepStart.format('LLL'), 'A', lastStepEnd.format('LLL'))
                L.push({
                    from: moment(previousStepStart),
                    to: moment(lastStepEnd)
                })

                lastStepEnd.subtract(1, this.selectedStepKey)
                previousStepStart.subtract(1, this.selectedStepKey)
            }

            this.steps = L.reverse()
        },
        getEntities () {
            this.apiError = false
            this.noResults = false
            this.notAuthorized = false

            if (this.userProfile) {
                // console.log('getEntities', this.entities)
                let sso_id = this.subject || this.userProfile.id

                // this.state.subject = this.subject.replace(/_/g, ' ') || this.userProfile.username.replace(/_/g, ' ')

                let from = this.lowerBound ? this.normalizeDate(this.lowerBound) : null
                let to = this.upperBound ? this.normalizeDate(this.upperBound): null
                let exclude = this.excludedTerms + '|' + '[a-z]+\\.[a-z\\.]+'
                exclude = exclude.replace(/\+/g, '%2B')

                this.xhrGetUrl = this.sfUrl + '?subject=' + sso_id

                // if (this.selectedBasinSlug) this.xhrGetUrl += '&basin_slug=' + this.selectedBasinSlug
                if (this.baitName) this.xhrGetUrl += '&topic=' + this.baitName
                if (this.useTimezone) this.xhrGetUrl += '&timezone=' + this.timezone
                if (from) this.xhrGetUrl += '&from=' + from
                if (to) this.xhrGetUrl += '&to=' + to

                this.xhrGetUrl += '&exclude=' + exclude

                this.performXhrGet()

            } else {
                // console.log('fetchin user profile')
                this.fetchUserProfile()
            }
        },
        getLicenseColor () { //(key) {
            return null
        },
        getNormalizedValue(value, maxValue) {
          return value / maxValue
        },
        getBasinUserValue(basinSlug, el) {
            // console.log('getBasinUserValue', basinSlug, el)
            if (el) {
                if (basinSlug == 'google-cse') {
                    let value = el.user.max_number_of_results ? el.user.max_number_of_results.value ? el.user.max_number_of_results.value : 0 : 0
                    if (this.normalizedState) {
                        return this.getNormalizedValue(value, this.getBasinMaxValue(basinSlug, el))
                    } else {
                        return value
                    }
                }

                let value = el.user.unique_url_count ? el.user.unique_url_count.value ? el.user.unique_url_count.value : 0 : 0
                if (this.normalizedState) {
                    return this.getNormalizedValue(value, this.getBasinMaxValue(basinSlug, el))
                } else {
                    return value
                }
            }
            return 0
        },
        getBasinMaxValue(basinSlug, el) {
            // console.log('getBasinMaxValue', basinSlug, el)
            if (basinSlug == 'google-cse') {
                return el.max_number_of_results ? el.max_number_of_results.value ? el.max_number_of_results.value : 0 : 0
            }
            return el.unique_url_count ? el.unique_url_count.value ? el.unique_url_count.value : 0 : 0
        },
        getBasinMinValue(basinSlug, el) {
            // console.log('getBasinMinValue', basinSlug, el)
            if (basinSlug == 'google-cse') {
                return el.min_number_of_result ? el.min_number_of_results.value ? el.min_number_of_results.value : 0 : 0
            }
            return 0
        },
        getBasinLicensesBuckets (data) {
            //console.log('getBasinLicensesBuckets', data,  this.selectedLicenseName)
            // filter user related licenses
            return data.aggregations.licenses.buckets.filter((l)=>{
                return l['by_interval']['buckets'].map((w) => {
                    return w['user']['doc_count']
                }).reduce((a, b) => a + b, 0) > 0
              })
        },
        calcLicenseSteps (license) {
            //console.log('calcLi', license)
            let buckets = license.by_interval.buckets
            let basinSlug = license.basin_slug.buckets[0].key
            //console.log('license buckets', buckets.length, 'basin slug', basinSlug)

            let points = []
            for (let i=0; i<buckets.length; i++) { //
                let el = buckets[i]
                // console.log('license', license, 'bucket', el)

                let relative_max_number_of_results = this.getBasinMaxValue(basinSlug, el)
                let relative_min_number_of_results = this.getBasinMinValue(basinSlug, el)
                //let val = el.user.max_number_of_results ? el.user.max_number_of_results.value : 0
                let val = this.getBasinUserValue(basinSlug, el)
                // console.log('VAL', val)

                let when = moment(this.normalizeDate(moment(el.key_as_string)))

                // console.log(el.key_as_string, 'WHEN', when.calendar(), 'VAL', val)

                points.push({
                    val: val,
                    when: when,
                    max: relative_max_number_of_results,
                    min: relative_min_number_of_results,
                    bucket: el,
                    license: license
                })
            }
            // console.log('points', points)
            // console.log('steps', this.steps.length)
            // this.steps.map((el)=>{console.log('EL', el, el.from ? el.from.calendar(): null)})

            let licenseSteps = []
            for (let i=0; i<this.steps.length; i++) {
                // cerco nei step quello che cade in ciascuno step
                let step = this.steps[i]
                let level = points.find((el) => {
                    // console.log('POINT', el)
                    // console.log(el.when ? el.when.calendar(): null, 'BETWEEN', el.when.isBetween(step.from, step.to))
                    let ok = el.when.isBetween(step.from, step.to, null, '[)')
                    // console.log(el.when.format('LLLL'), 'is between', step.from.format('LLLL'), 'and', step.to.format('LLLL'), '?', el.when.isBetween(step.from, step.to, null, '[)'))
                    return ok
                })
                if (level) {
                    // console.log('found level', level)
                    licenseSteps.push(this.getStepChartPoint(step, level))
                } else {
                    // console.log('level not found 4 step', i, step)
                    licenseSteps.push(this.getStepChartPoint(step))
                }
                // console.log('licenseSteps', licenseSteps)
            }

            // console.log('license licenseSteps', license.key, licenseSteps)
            return licenseSteps
        },
        updateChartCategories () {
            this.categories = this.steps.map((el) => {
                return el.from.format(this.dateFormat) + ' - ' + el.to.format(this.dateFormat)
            })
        },
        getStepChartPoint (step, level, empty) {
            // console.log('getStepChartPoint', level, empty)
            let bucket = level ? level.bucket : null
            let license = level ? level.license : null
            let when = level ? level.when : null

            let color = 'silver'

            if (level) {
                let max = null
                let val = level.val ? level.val : 0

                if (level.max && level.min) {
                    max = level.max - level.min

                    val = level.val - level.min
                    val = val / max
                    val = ( Math.round(val * 100) / 100 )
                }

                color = this.getLevelColor(val)
                return {y: val, val: level.val, max: level.max ? level.max : null, min: level.min ? level.min : null, empty: empty, color: color, license: license, bucket: bucket, when: when, step: step}
            } else {
                return {y:0, val: 0, max: 0, min: 0, empty: empty, color: color, license: license, bucket: bucket, when: when, step: step}
            }
        },
        getChartOptions () { // returns Highcharts chart Object
            let self = this
            // console.log('getChartOptions', this.entities)

            this.showLatestArticles = false
            this.selectedPoints = []
            this.wordList = []

            let basinLicensesBuckets = this.getBasinLicensesBuckets(this.entities)
            if (basinLicensesBuckets == null || basinLicensesBuckets.length == 0) return

            let LEVELS = []
            let series = []

            for (let licenseC=0; licenseC<basinLicensesBuckets.length; licenseC++) {
                let license = basinLicensesBuckets[licenseC]
                // console.log('license', license)

                let licenseSteps = this.calcLicenseSteps(license)

                LEVELS.push(licenseSteps)

                let serie = {
                    allowPointSelect: true,
                    states: {
                        select: {
                            color: 'rgb(0, 190, 164)',
                            borderColor: 'black',
                            dashStyle: 'dot'
                        }
                    },
                    point: {
                        events: {
                            select: function (ev) {
                                setTimeout(() => {
                                    console.log('SELECT', ev)
                                    if (ev.accumulate) {
                                        self.selectedPoints.push(this);
                                    } else {
                                        self.selectedPoints = [this];
                                    }
                                    self.updateWordList()
                                }, 0)
                            },
                            unselect: function () {
                                setTimeout(() => {
                                    // console.log('UNSELECT', this)
                                    let _self = this
                                    self.selectedPoints = self.selectedPoints.filter(function (p) {
                                        return p.id !== _self.id
                                    })
                                    self.updateWordList()
                                }, 0)
                            }
                        }
                    },
                    name: license.key,
                    data: LEVELS[licenseC],
                    color: this.getLicenseColor(license.key)
                }
                series.push(serie)
                //console.log('LEVELS', LEVELS[0])
                // LEVELS[0].map((el)=>{console.log(el.when ? el.when.calendar(): null, el.val)})
            }

            // console.log('SERIES', series)

            let titleText = this.state.asList ? (this.selectedLicenseName ? this.selectedLicenseName : null) : null

            let _chartOptions = {
                chart: {
                    type: 'column'
                },
                title: {
                    text: titleText
                },
                xAxis: {
                    categories: this.categories,
                    labels: {
                        enabled: !this.state.asList
                    }
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: this.selectedBasinSlug == 'google-cse' ? 'Total pages' : 'Total links'
                    },
                    stackLabels: {
                        enabled: true,
                        style: {
                            fontWeight: 'bold',
                            color: ( // theme
                                Highcharts.defaultOptions.title.style &&
                                Highcharts.defaultOptions.title.style.color
                            ) || 'gray'
                        }
                    }
                },
                legend: {
                    backgroundColor:
                        Highcharts.defaultOptions.legend.backgroundColor || 'white',
                    borderColor: '#CCC',
                    borderWidth: 1,
                    shadow: false,
                    enabled: !this.state.asList
                },
                tooltip: {
                    headerFormat: '<b>{point.x}</b><br/>',
                    pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
                },
                plotOptions: {
                    column: {
                        stacking: 'normal',
                        dataLabels: {
                            enabled: true
                        }
                    },
                    series: {
                        events: {
                            legendItemClick: function(e) {
                                /*if (!this.visible) {
                                  console.log('selected')
                                } else {
                                  console.log('unselected')
                                } */
                                self.wordList = []
                                for (let i=0; i<self.selectedPoints.length; i++) {
                                    let point = self.selectedPoints[i]
                                    point.select(false)
                                }
                                self.selectedPoints = []
                            }
                        }
                    }
                },
                series: series
            }

            this.updateTrend()

            return _chartOptions
        },
        updateWordList () {
            // console.log('updateWordList')
            this.wordList = []

            let _wordList = {}
            let wordList = []
            for (let i=0; i<this.selectedPoints.length; i++) {
                let point = this.selectedPoints[i]
                let mUTs = point.bucket.user.most_used_terms.buckets
                for (let j=0; j<mUTs.length; j++) {
                    let el = mUTs[j]
                    if (_wordList[el.key]) {
                        _wordList[el.key] += el.doc_count
                    } else {
                        _wordList[el.key] = el.doc_count
                    }
                }
                for (let prop in _wordList) {
                    if (Object.prototype.hasOwnProperty.call(_wordList, prop)) {
                        wordList.push({
                            name: prop,
                            weight: _wordList[prop]
                        })
                    }
                }
                this.wordList = wordList
            }
        },
        timelineUpdated (tS) {
            console.log('timelineUpdated', tS)
            this.shift = tS.shift

            //console.log('shift changed')
            this.getEntities()
        },
        parseEntities (data) {
            // console.log('parseEntities', data)
            this.updateLicensesNames(data)

            this.update()

            return data
        }
    },
    watch: {
        entities (n) {
            // console.log('entities changed', n)
            if (n) {
                let _chartOptions = this.getChartOptions()
                // console.log('ChartOptions', JSON.stringify(_chartOptions))
                this.chartOptions = _chartOptions
            }
        },
        asList (n) {
            this.state.asList = n
        },
        asListState () {
            this.chartOptions = this.getChartOptions()
        },
        normalizedState () {
            this.chartOptions = this.getChartOptions()
        },
        selectedPoints () {
            this.showLatestArticles = false
        }
    }
}
