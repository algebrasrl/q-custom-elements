/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue'

import RumorList from './RumorList'
import notes from './RumorList.md'


const template = `
<div class="contenitore" style="background-color: #fefefe;">
    <RumorList
      auto />
</div>
`

storiesOf('rumor', module)
  .add('RumorList', () => ({
    components: { RumorList },
    template: template
  }), {
    notes
  })
