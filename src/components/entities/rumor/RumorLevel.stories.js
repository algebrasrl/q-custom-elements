/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue'

import RumorLevel from './RumorLevel'
import notes from './RumorLevel.md'


const template = `
<div class="contenitore" style="background-color: #fefefe;">
    <RumorLevel
        subject="Provincia di Brescia"
        topic="inquinamento"
        level="0.89"
        color="#f00"
        />
</div>
`

storiesOf('rumor', module)
  .add('RumorLevel', () => ({
    components: { RumorLevel },
    template: template
  }), {
    notes
  })
