/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue'

import RumorTrend from './RumorTrend'
import notes from './RumorTrend.md'


const template = `
<div class="contenitore" style="background-color: #fefefe;">
  <div>
    <RumorTrend trend="1" />
  </div>
  <div>
    <RumorTrend is-negative trend="1" />
  </div>
</div>
`

storiesOf('rumor', module)
  .add('RumorTrend', () => ({
    components: { RumorTrend },
    template: template
  }), {
    notes
  })
