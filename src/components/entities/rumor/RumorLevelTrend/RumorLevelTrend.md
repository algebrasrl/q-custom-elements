RumorLevelTrend

## Examples

Default:

```jsx
<RumorLevelTrend
  auto
  topic="odore"
  subject="odore"
  subject-name="Odore"
  topic-name="Odore"
  />
```
