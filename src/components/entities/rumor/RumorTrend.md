RumorTrend

## Examples

Default:

```jsx
<RumorTrend trend="1" />
```

isNegative:

```jsx
<RumorTrend
    is-negative
    trend="1" />
```
