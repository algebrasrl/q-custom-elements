/**
 * @mixin
 * Defines the base common interface to interact with rumor related backends.
 */

import DjangoSsoAppUserElementMixin from '@paiuolo/django-sso-app-vue-mixins/mixins/DjangoSsoAppUserElementMixin'

import EntityListMixin from '@paiuolo/pai-vue-mixins/mixins/EntityListMixin'
import xhrGetMixin from '@paiuolo/pai-vue-mixins/mixins/xhrGetMixin'


export default {
  mixins: [
    EntityListMixin,
    xhrGetMixin,
    DjangoSsoAppUserElementMixin
  ],
  props: {
    auto: {
      type: Boolean,
      default: false
    },
    normalized: {
      type: Boolean,
      default: false
    },
    hideLoginMask: {
      type: Boolean,
      default: false
    },
    userProfileUrl: {
      type: String,
      default: 'https://accounts.q-cumber.org/api/v1/auth/profile/'
    },
    loginUrl: {
      type: String,
      default: 'https://accounts.q-cumber.org/login/'
    },
    sfUrl: {
      type: String,
      default: 'https://pescatore.q-cumber.org/serverless-functions/rumor-list/'
    },
    sfAction: {
      type: String,
      default: 'get'
    },
    size: {
      type: [String, Number],
      default: 10
    },
    start: {
      type: [String, Number],
      default: 0
    },
    userId: {
      type: String,
      default: null
    },

    useTimezone: {
      type: Boolean,
      default: false
    },
    startDate: {
      type: Object,
      default: null
    },
    endDate: {
      type: Object,
      default: null
    },
    basins: {
      type: Array,
      default () {
        return [
          {
            name: 'Google CSE',
            slug: 'google-cse'
          },
          {
            name: 'RSS',
            slug: 'rss'
          },
          {
            name: 'Twitter',
            slug: 'twitter'
          }
        ]
      }
    },
    basinSlug: {
      type: String,
      default: null
    },
    timezone: {
      type: String,
      default () {
        let offset = (((new Date()).getTimezoneOffset()/60)*-1)
        let sign = (offset >= 0) ? '+' : '-'
        let as_str = String(offset)
        if (as_str.length == 1) as_str = '0' + as_str
        let tz = sign + as_str + ':00'

        return encodeURIComponent(tz)
      }
    },
    licenseName: {
       type: String,
       default: null
    },
    extraArgs: {
      type: String,
      default: null
    }
  },
  data () {
    return {
      collapseUrl: true,
      loadFrom: parseInt(this.start),
      maxFound: 0,
      apiError: false,
      notAuthorized: false,

      xhrGetParams: {
        withCredentials: true
      },
      xhrGetPermittedErrors: '502',
      xhrGetMaxRetries: 5,
      xhrGetRetryTimeout: 1999,
      xhrGetRetryOnPermittedError: true,

      // licenseName: null,
      baitName: this.topic,

      state: {
        selectedBasinSlug: this.basins[0].slug,
        subject: this.subject,
        asList: this.asList,
        normalized: this.normalized
      }
    }
  },
  mounted () {
    this.$on('performingXhrGet', () => {
      // console.log('performingXhrGet')

      this.startLoading()

      this.$emit('performingGetEntities')
    })
    this.$on('xhrGetResponse', (res) => {
      // console.log('xhrGetResponse', res)
      this.entities = this.parseEntities(res.data)

      this.$emit('getEntitiesSuccess', res.data)
      this.stopLoading()
    })
    this.$on('xhrGetError', (err) => {
      // console.log('xhrGetError')
      if (err.response && err.response.status === 403) {
        this.notAuthorized = true
      } else {
        this.apiError = true
        this.$emit('getEntitiesError', err)
      }
      this.stopLoading()
    })

    this.$on('userIsAnonymous', ()=>{
      this.stopLoading()
    })

    if (this.auto) {
      this.getEntities()
    }
  },
  computed: {
    selectedBasin () {
      let basins = this.basins.filter((b) => {
          return b.slug === this.state.selectedBasinSlug
        })
      let sel = basins.length ? basins [0] : null
      return sel
    },
    selectedBasinName () {
      let name = this.selectedBasin ? this.selectedBasin.name : null
      return name
    },
    selectedBasinSlug () {
      return this.state.selectedBasinSlug
    },
    asListState () {
      return this.state.asList
    },
    normalizedState () {
      return this.state.normalized
    }
  },
  methods: {
    getBasinIndicator (basinSlug) {
      if (basinSlug === 'google-cse') {
        return 'max_number_of_results'
      } else {
        return 'unique_url_count'
      }
    },
    getBasinName (basinSlug) {
      if (basinSlug === 'google-cse') {
        return 'Google CSE'
      } else if (basinSlug === 'twitter') {
        return 'Twitter'
      } else if (basinSlug === 'rss') {
        return 'RSS'
      }
      return 'Basin'
    },
    getLevelColor (x) {
      // console.log('COLOR', x, this.state.normalized)
      let color
      if (this.state.normalized) {
        let a = this.isNegative ? (1-x) : x
        let hue = a * 120
        color = "hsl(" + hue + ", 90%, 50%)"
      }
      // console.log('FOTERTR,', color)
      return color
    },
    getBucketCount (B, s) {
      for (let i=0; i<B.length; i++) {
        let b = B[i]
        if (b.key === s) {
          return b.doc_count
        }
      }
      return 0
    },
    parseEntities (data) {
      this.maxFound = data.hits.total.value

      let e = data.hits.hits.map((e) => {
        return e['_source']
      })

      if (this.loadFrom > 0) {
        e = this.entities.concat(e)
      }

      return e
    },
    normalizeDate (d) {
      if (this.useTimezone) {
        return d
      } else {
        return d.toISOString()
      }
    },
    getEntities () {
      // console.log('getEntities', this.licenseName)
      this.apiError = false
      this.notAuthorized = false  

      if (this.userProfile) {
        let action = this.sfAction
        if (this.userId) action = 'search'
        this.xhrGetUrl = this.sfUrl + '?action=' + action

        if (this.loadFrom) this.xhrGetUrl += '&start=' + this.loadFrom
        if (this.size) this.xhrGetUrl += '&size=' + parseInt(this.size)
        if (this.useTimezone) this.xhrGetUrl += '&timezone=' + this.timezone
        if (this.startDate) this.xhrGetUrl += '&from=' + this.normalizeDate(this.startDate)
        if (this.endDate) this.xhrGetUrl += '&to=' + this.normalizeDate(this.endDate)
        if (this.userId) this.xhrGetUrl += '&user_id=' + this.userId
        if (this.basinSlug) this.xhrGetUrl += '&basin_slug=' + this.basinSlug
        if (this.licenseName) this.xhrGetUrl += '&license_name=' + this.licenseName
        if (this.baitName) this.xhrGetUrl += '&bait_name=' + this.baitName
        if (this.collapseUrl) this.xhrGetUrl += '&collapse_url'

        this.performXhrGet()

      } else {
        this.fetchUserProfile()
      }
    },
    loadMore () {
      this.loadFrom += parseInt(this.size)
      this.getEntities()
    },
    hasMore () {
      let asksFor = this.loadFrom + parseInt(this.size)
      let shoudHave = this.loadFrom
      let has = this.entities.length
      // console.log('asksFor', asksFor, 'shoudHave', shoudHave, 'has', has)
      return this.maxFound > 0 && asksFor < this.maxFound && (has - shoudHave) >= 0
    }
  },
  watch: {
    auto (n) {
      // console.log('auto changed', n)
      if (n) {
        this.getEntities()
      }
    },
    userId (n) {
      // console.log('userId changed', n)
      if (n && this.auto) {
        this.getEntities()
      }
    },
    userProfile (n, o) {
      if (((n && o == null) || (n && o && n.sso_id !== o.sso_id) ) && this.auto) {
        // console.log('userProfile changed', n, o)
        this.setLocale(n.language)

        this.getEntities()
      }
    },
    basinSlug (n) { // , o) {
      if (n) {
        // console.log('slug changed', o, n)
        this.state.selectedBasinSlug = n
        this.getEntities()
      }
    }
  }
}
