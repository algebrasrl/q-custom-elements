import moment from 'moment'
import {Chart} from 'highcharts-vue'


export default {
  components: {
    highcharts: Chart
  },
  props: {
    normalized: {
      type: Boolean,
      default: false
    },
    subject: {
      type: String,
      default: null
    },
    topic: {
      type: String,
      default: null
    },
    subjectName: {
      type: String,
      default: null
    },
    topicName: {
      type: String,
      default: null
    },
    selectedLicense: {
      type: String,
      default: null
    },
    hideSubject: {
      type: Boolean,
      default: false
    },
    hideTopic: {
      type: Boolean,
      default: false
    },
    enableExporting: {
      type: Boolean,
      default: false
    },
    isNegative: {
      type: Boolean,
      default: true
    },
    asList: {
      type: Boolean,
      default: false
    }
  },
  data () {
    return {
      showChart: true,
      chartLoaded: false,
      asListD: this.asList,
      licensesNames: [],
      selectedLicenseName: this.selectedLicense
    }
  },
  computed: {
    subjectTitle () {
        return this.subjectName || this.subject || 'undefined'
    },
    topicTitle () {
        return this.topicName || this.topic || 'undefined'
    },
  },
  methods: {
    update () {
      // Should be overriden
    },
    getEntityUrl (e) {
      return e.doc.meta.url
    },
    getEntityIcon () {
      return 'link'
    },
    getEntityTitle (e) {
      if (e) {
        if (e.doc.meta['django-fisherman'].basin.slug == 'twitter') {
          return e.doc.meta.twitter.full_text
        }
        return e.doc.meta.title.replace(/_/g, ' ')
      }
    },
    getEntityText (e) {
      return e.corpo
    },
    getEntityIconStyle () {
      let style = {}

      if (this.iconColor) {
        style['color'] = this.iconColor
      }
      return style
    },
    getEntityDate (e) {
      let date = new Date(e.created_at)
      return moment(date).calendar()
    },
    getLevelColor (x) {
      if (this.normalized) {
        let a = this.isNegative ? (1-x) : x
        let hue = a * 120
        return "hsl(" + hue + ", 90%, 50%)"
      }

      return this.iconColor
    },
    highchartsCallback () {
      this.chartLoaded = true
    },
    updateLicensesNames (data) {
      this.licensesNames = data['aggregations']['licenses']['buckets'].map((l)=>{
        return l['key']
      })
      this.selectedLicenseName = this.selectedLicense || this.licensesNames[0]
    }
  },
  watch: {
    asList (n) {
      this.asListD = n
    },
    subject () {
      this.update()
    },
    topic () {
      this.update()
    },
    showChart (n) {
      if (n) {
        return
      } else {
        this.chartLoaded = false
      }
    }
  }
}
