/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue'

import RumorWordCloud from './RumorWordCloud'
import notes from './RumorWordCloud.md'


const template = `
<div class="contenitore" style="background-color: #fefefe; position: relative; height: 500px;">
  <RumorWordCloud
    text="L'inquinamento del territorio è un tema molto importante. Importante."
  />
</div>
`

const templateAsList = `
  <RumorWordCloud
    as-list
    text="L'inquinamento del territorio è un tema molto importante. Importante."
  />
`


storiesOf('rumor.RumorWordCloud', module)
  .add('default', () => ({
    components: { RumorWordCloud },
    template: template,
    data () {
      return {
      }
    },
    methods: {
    }
  }), {
    notes
  }).add('as-list', () => ({
    components: { RumorWordCloud },
    template: templateAsList,
    data () {
      return {
      }
    },
    methods: {
    }
  }), {
    notes
  })
