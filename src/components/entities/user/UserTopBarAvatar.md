UserTopBarAvatar

## Properties

- auto

    Automatically loads user profile

- right-aligned

    The component will be placed inside containers close to the right border

## Examples

Default:

```jsx
<UserTopBarAvatar auto />
```

With slotted content:

```jsx
<UserTopBarAvatar>
  <li class="mdc-list-item segnala-top-button" role="menuitem" onclick="location.href='https://www.q-cumber.org'"><a href="https://www.q-cumber.org" alt="Q-cumber" style="color: #111; text-decoration: none; width: 100%;"><div>Segnala su <br> <b style="color: #00bea4;">Q-cumber</b></div></a></li>
</UserTopBarAvatar>
```

External user profile:

```jsx
<UserTopBarAvatar 
  :external-user-profile="globalUserProfile" />
```

