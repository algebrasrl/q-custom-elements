UserCard

## Examples

Default:

```jsx
<UserCard></UserCard>
```

External user profile:

```jsx
<UserCard
  :external-user-profile="globalUserProfile"
  >
</UserCard>
```

Narrow:

```jsx
<UserCard
  narrow
  :external-user-profile="globalUserProfile"
  >
</UserCard>
```
