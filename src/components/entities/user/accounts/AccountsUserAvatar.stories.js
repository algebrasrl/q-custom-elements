/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue'

import AccountsUserAvatar from './AccountsUserAvatar'
import notes from './AccountsUserAvatar.md'


const template = `
<div class="contenitore">
    <AccountsUserAvatar />
</div>
`

storiesOf('accounts', module)
  .add('AccountsUserAvatar', () => ({
    components: { AccountsUserAvatar },
    template: template
  }), {
    notes
  })
