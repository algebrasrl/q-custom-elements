/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue'

import AccountsUserAvatarUploadField from './AccountsUserAvatarUploadField'
import notes from './AccountsUserAvatarUploadField.md'


const template = `
<div class="contenitore">
    <AccountsUserAvatarUploadField />
</div>
`

storiesOf('accounts', module)
  .add('AccountsUserAvatarUploadField', () => ({
    components: { AccountsUserAvatarUploadField },
    template: template
  }), {
    notes
  })
