/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue'

import UserCard from './UserCard'
import notes from './UserCard.md'


const template = `
<div class="contenitore">
    <UserCard auto />
</div>
`

storiesOf('user', module)
  .add('UserCard', () => ({
    components: { UserCard },
    template: template
  }), {
    notes
  })
