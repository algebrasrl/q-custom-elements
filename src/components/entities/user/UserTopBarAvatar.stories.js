/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue'
import { action } from '@storybook/addon-actions'

import UserTopBarAvatar from './UserTopBarAvatar'
import notes from './UserTopBarAvatar.md'


const template = `
<div class="contenitore" style="text-align: center; height: 220px;">
    <UserTopBarAvatar @menuOpened="menuOpened" @menuClosed="menuClosed" auto>
        <li class="mdc-list-item segnala-top-button" role="menuitem" onclick="location.href='https://www.q-cumber.org'">
            <a href="https://www.q-cumber.org" alt="Q-cumber" style="color: #111; text-decoration: none; width: 100%;">
                <div>Segnala su <br> <b style="color: #00bea4;">Q-cumber</b></div>
            </a>
        </li>
    </UserTopBarAvatar>
</div>
`

storiesOf('user', module)
  .add('UserTopBarAvatar', () => ({
    components: { UserTopBarAvatar },
    template: template,
    methods: {
      menuOpened: action('menuOpened'),
      menuClosed: action('menuClosed')
    }
  }), {
    notes
  })
