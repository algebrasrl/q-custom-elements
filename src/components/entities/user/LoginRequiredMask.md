LoginRequiredMask

## Examples

Default:

```jsx
<div style="position: relative; overflow: hidden; height: 120px;">
  <LoginRequiredMask>
  </LoginRequiredMask>
</div>
```
