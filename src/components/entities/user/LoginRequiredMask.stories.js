/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue'
import { action } from '@storybook/addon-actions'

import LoginRequiredMask from './LoginRequiredMask'
import notes from './LoginRequiredMask.md'


const template = `
<div class="contenitore" style="height: 120px;">
    <LoginRequiredMask
        @performingFetchUserProfile="performingFetchUserProfile" />
</div>
` 

storiesOf('user', module)
  .add('LoginRequiredMask', () => ({
    components: { LoginRequiredMask },
    template: template,
    methods: {
      performingFetchUserProfile: action('performingFetchUserProfile')
    }
  }), {
    notes
  })
