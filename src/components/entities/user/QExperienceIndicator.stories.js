/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue'
import { action } from '@storybook/addon-actions'

import QExperienceIndicator from './QExperienceIndicator'
import notes from './QExperienceIndicator.md'


const template = `
<div class="contenitore">
    <QExperienceIndicator
        experience="0.55" />
</div>
` 

storiesOf('user', module)
  .add('QExperienceIndicator', () => ({
    components: { QExperienceIndicator },
    template: template,
    methods: {
      click: action('click')
    }
  }), {
    notes
  })
