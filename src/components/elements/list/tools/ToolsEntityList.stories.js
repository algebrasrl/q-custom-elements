/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue'

import ToolsEntityList from './ToolsEntityList'
import notes from './ToolsEntityList.md'


const template = `
<div class="contenitore tools">
    <ToolsEntityList
        :entities="[
          {
          'slug': 'one',
          'name': 'One'
          },
          {
          'slug': 'two',
          'name': 'Two'
          },
          {
          'slug': 'three',
          'name': 'Three'
          },
        ]"
        selected-slug="three" />
</div>
`

storiesOf('tools', module)
  .add('ToolsEntityList', () => ({
    components: { ToolsEntityList },
    template: template
  }), {
    notes
  })
