/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue'

import ToolsEntitySelectableList from './ToolsEntitySelectableList'
import notes from './ToolsEntitySelectableList.md'


const template = `
<div class="contenitore tools">
    <ToolsEntitySelectableList
        :entities="[
          {
          'slug': 'one',
          'name': 'One'
          },
          {
          'slug': 'two',
          'name': 'Two'
          },
          {
          'slug': 'three',
          'name': 'Three'
          },
        ]"
        selected-slug="three" />
</div>
`

storiesOf('tools', module)
  .add('ToolsEntitySelectableList', () => ({
    components: { ToolsEntitySelectableList },
    template: template
  }), {
    notes
  })
