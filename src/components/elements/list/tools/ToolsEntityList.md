ToolsEntityList

## Examples

### Default

```jsx
<ToolsEntityList
  :entities="[
    {
    'slug': 'one',
    'name': 'One'
    },
    {
    'slug': 'two',
    'name': 'Two'
    },
    {
    'slug': 'three',
    'name': 'Three'
    },
  ]"
  selected-slug="three"
  />

```