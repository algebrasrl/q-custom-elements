/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue'

import LoadingDots from './LoadingDots'
import notes from './LoadingDots.md'


const template = `
<div class="contenitore">
    <LoadingDots /> <br/>
    <LoadingDots dark/>
</div>
`

storiesOf('loader', module)
  .add('LoadingDots', () => ({
    components: { LoadingDots },
    template: template
  }), {
    notes
  })
