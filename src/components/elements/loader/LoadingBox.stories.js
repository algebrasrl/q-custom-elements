/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue'

import LoadingBox from './LoadingBox'
import notes from './LoadingBox.md'


const template = `
<div class="contenitore" style="height: 240px;">
  <div style="height: 120px; position: relative;">
    <LoadingBox />
  </div>

  <div style="height: 120px; position: relative; background-color: #000;">
    <LoadingBox 
      dark />
  </div>
</div>
`

storiesOf('loader', module)
  .add('LoadingBox', () => ({
    components: { LoadingBox },
    template: template
  }), {
    notes
  })
