LoadingBox

## Examples

### Default

```jsx
<div style="position: relative; height: 120px;">
  <LoadingBox />
</div>
```

### Dark

```jsx
<div style="position: relative; height: 120px; background-color: #000;">
  <LoadingBox dark />
</div>
```
