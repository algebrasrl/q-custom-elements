LoadingDots

## Examples

### Default

```jsx
<LoadingDots />
```

### Dark

```jsx
<div style="background-color: #111">
  <LoadingDots
    dark />
</div>
```
