/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue'

import DateIntervalSlider from './DateIntervalSlider'
import notes from './DateIntervalSlider.md'


const template = `
<div class="contenitore">
    <DateIntervalSlider
        begin-date="now-6M" />
</div>
`

storiesOf('slider', module)
  .add('DateIntervalSlider', () => ({
    components: { DateIntervalSlider },
    template: template
  }), {
    notes
  })
