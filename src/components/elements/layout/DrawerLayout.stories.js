/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue'

import DrawerLayout from './DrawerLayout'


const template = `
<div class="contenitore" style="height: 100vh; padding: 0;">
    <DrawerLayout />
</div>
`


storiesOf('drawer', module).add('drawer-list', () => ({
    components: { DrawerLayout },
    template: template
  }), {})

