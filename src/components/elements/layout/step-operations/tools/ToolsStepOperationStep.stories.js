/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue'

import ToolsStepOperationStep from './ToolsStepOperationStep'
import ToolsStepOperationInstructions from './ToolsStepOperationInstructions'
import ToolsButton from '../../../button/tools/ToolsButton'
import notes from './ToolsStepOperationStep.md'


const template = `
<div class="contenitore" style="height: 710px;">
  <ToolsStepOperationStep
    :hidden="false"
    :presentation="presentation"
   >

    <h1>Instructions</h1>
    <form>
    <input id="presentationMode" type="checkbox" v-model="presentation" />
    <label for="presentationMode">"Presentation" mode</label>
    </form>

    <ToolsStepOperationInstructions
      :step-operation="stepOperation"/>

    <ToolsButton slot="actions">left</ToolsButton>

    <ToolsButton slot="actions"
      align-right
      half-width>right</ToolsButton>

  </ToolsStepOperationStep>
</div>
`

storiesOf('tools', module)
  .add('ToolsStepOperationStep', () => ({
    components: { ToolsStepOperationStep, ToolsButton, ToolsStepOperationInstructions },
    template: template,
    data () {
      return {
        presentation: false,
        stepOperation: {
          key: 'rumor-filter-operation',
          name: 'Rumor dashboards operation',
          description: 'Uploads a file.',
          activeStep: null,
          steps: [
            {
              key: 'one',
              name: 'One',
              description: 'step one',
              next: null,
              result: null,
              error: null
            }
          ]
        }
      }
    }
  }), {
    notes
  })
