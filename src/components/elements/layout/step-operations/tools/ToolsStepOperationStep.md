FriendlyIframe

## Examples

### Default

```jsx
<ToolsStepOperationStep
  :hidden="false" >

  <h1>Instructions</h1>
  
  <ToolsStepOperationInstructions
    :step-operation="stepOperation"/>
   
  <ToolsButton slot="actions">left</ToolsButton>

  <ToolsButton slot="actions"
    align-right
    half-width>right</ToolsButton>

</ToolsStepOperationStep>
``` 
