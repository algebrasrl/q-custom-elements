/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue'

import FriendlyIframe from './FriendlyIframe'
import notes from './FriendlyIframe.md'


const template = `
<div class="contenitore" style="height: 480px;">
    <FriendlyIframe
        url="https://tools.q-cumber.org/file-upload/" />
</div>
`

storiesOf('iframe', module)
  .add('FriendlyIframe', () => ({
    components: { FriendlyIframe },
    template: template
  }), {
    notes
  })
