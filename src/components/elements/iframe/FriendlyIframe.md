FriendlyIframe

## Examples

### Default

```jsx
<div style="position: relative; height: 480px;">
  <FriendlyIframe
    url="https://tools.q-cumber.org/file-upload/" />
</div>
``` 
