/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue'

import LoyaltyMdcTextarea from './LoyaltyMdcTextarea'
import notes from './LoyaltyMdcTextarea.md'


const template = `
<div class="contenitore white">
    <LoyaltyMdcTextarea 
        content="text" />
        
    <div style="background-color: #111;">
        <LoyaltyMdcTextarea
        dark
        content="text" />
    </div>
</div>
`

storiesOf('loyalty', module)
  .add('LoyaltyMdcTextarea', () => ({
    components: { LoyaltyMdcTextarea },
    template: template
  }), {
    notes
  })
