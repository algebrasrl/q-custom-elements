LoyaltyMdcTextarea

## Examples

### Default

```jsx
<LoyaltyMdcTextarea
  content="text"
>
</LoyaltyMdcTextarea>
```

### Dark

```jsx
<div style="background-color: #111; padding: 16px;">
  <LoyaltyMdcTextarea
    content="text"
    dark>
  </LoyaltyMdcTextarea>
</div>
```
