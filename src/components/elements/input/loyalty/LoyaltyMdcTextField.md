LoyaltyMdcTextField

## Examples

### Default

```jsx
<LoyaltyMdcTextField
  content="text">
</LoyaltyMdcTextField>
```

### Dark

```jsx
<div style="background-color: #111; padding: 16px;">
  <LoyaltyMdcTextField
    content="text"
    dark>
  </LoyaltyMdcTextField>
</div>
```