/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue'

import LoyaltyMdcTextField from './LoyaltyMdcTextField'
import notes from './LoyaltyMdcTextField.md'


const template = `
<div class="contenitore white">
    <LoyaltyMdcTextField
        content="text"/>

    <div style="background-color: #111;">
        <LoyaltyMdcTextField
        dark
        content="text" />
    </div>
</div>
`

storiesOf('loyalty', module)
  .add('LoyaltyMdcTextField', () => ({
    components: { LoyaltyMdcTextField },
    template: template
  }), {
    notes
  })
