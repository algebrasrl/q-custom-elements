/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue'

import AccountsMdcTextarea from './AccountsMdcTextarea'
import notes from './AccountsMdcTextarea.md'


const template = `
<div class="contenitore">
    <AccountsMdcTextarea
      content="text" />
</div>
`

storiesOf('accounts', module)
  .add('AccountsMdcTextarea', () => ({
    components: { AccountsMdcTextarea },
    template: template
  }), {
    notes
  })
