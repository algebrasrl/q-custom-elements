/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue'

import AccountsMdcPlaceInput from './AccountsMdcPlaceInput'
import notes from './AccountsMdcPlaceInput.md'


const template = `
<div class="contenitore">
    <AccountsMdcPlaceInput />
</div>
`

storiesOf('accounts', module)
  .add('AccountsMdcPlaceInput', () => ({
    components: { AccountsMdcPlaceInput },
    template: template
  }), {
    notes
  })
