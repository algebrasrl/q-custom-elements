/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue'

import AccountsMdcTextField from './AccountsMdcTextField'
import notes from './AccountsMdcTextField.md'


const template = `
<div class="contenitore">
    <AccountsMdcTextField
      content="Text"/>
</div>
`

storiesOf('accounts', module)
  .add('AccountsMdcTextField', () => ({
    components: { AccountsMdcTextField },
    template: template
  }), {
    notes
  })
