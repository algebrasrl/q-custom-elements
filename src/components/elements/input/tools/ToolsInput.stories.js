/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue'

import ToolsInput from './ToolsInput'
import notes from './ToolsInput.md'


const template = `
<div class="contenitore tools">
    <ToolsInput
       content="text"/>
</div>
`

storiesOf('tools', module)
  .add('ToolsInput', () => ({
    components: { ToolsInput },
    template: template
  }), {
    notes
  })
