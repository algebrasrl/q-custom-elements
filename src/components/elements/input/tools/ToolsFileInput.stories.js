/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue'

import ToolsFileInput from './ToolsFileInput'
import notes from './ToolsFileInput.md'


const template = `
<div class="contenitore tools">
    <ToolsFileInput />
</div>
`

storiesOf('tools', module)
  .add('ToolsFileInput', () => ({
    components: { ToolsFileInput },
    template: template
  }), {
    notes
  })
