/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue'

import ToolsPlaceInput from './ToolsPlaceInput'
import notes from './ToolsPlaceInput.md'


const template = `
<div class="contenitore tools">
    <ToolsPlaceInput />
</div>
`

storiesOf('tools', module)
  .add('ToolsPlaceInput', () => ({
    components: { ToolsPlaceInput },
    template: template
  }), {
    notes
  })
