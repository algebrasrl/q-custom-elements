/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue'

import ToolsTextarea from './ToolsTextarea'
import notes from './ToolsTextarea.md'


const template = `
<div class="contenitore tools">
    <ToolsTextarea
      content="text" />
</div>
`

storiesOf('tools', module)
  .add('ToolsTextarea', () => ({
    components: { ToolsTextarea },
    template: template
  }), {
    notes
  })
