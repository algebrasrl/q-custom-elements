QEventsMap

## Properties

- event-types

    List (csv) of displayed event types

- size

    Limit number of events (default 200)

- hide-avatar
    
    Keeps user avatar hidden


## Examples

### Default

```jsx
<div style="position: relative; height: 420px;">
  <QEventsMap
      auto
      event-types="qpost_verde,qpost_giallo,qpost_grigio,qpost_rosso,qpost_rosso_ticket,odour_alert" />
</div>
```
