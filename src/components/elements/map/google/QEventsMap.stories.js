/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue'

import QEventsMap from './QEventsMap'
import notes from './QEventsMap.md'


const template = `
<div class="contenitore" style="height: 480px; padding: 0;">
    <QEventsMap
      auto
      event-types="qpost_verde,qpost_giallo,qpost_grigio,qpost_rosso,qpost_rosso_ticket,odour_alert"
      />
</div>
`

const template_phone = `
<div class="contenitore" style="width: 320px; height: 568px; border: 4px solid #111; border-bottom: 24px solid #111; border-radius: 16px; background: #111;">
    <QEventsMap
      auto
      event-types="qpost_verde,qpost_giallo,qpost_grigio,qpost_rosso,qpost_rosso_ticket,odour_alert"
      />
</div>
`


storiesOf('map.goole', module).add('events', () => ({
    components: { QEventsMap },
    template: template
  }), {
    notes
  }).add('events-phone-layout', () => ({
    components: { QEventsMap },
    template: template_phone
  }), {
    notes
  })

