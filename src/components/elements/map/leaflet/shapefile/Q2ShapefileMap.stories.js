/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue'

import Q2ShapefileMap from './Q2ShapefileMap'


const template = `
<div class="contenitore" style="height: 100vh; padding: 0;">
    <Q2ShapefileMap
      :source-url="sourceUrl"
      feature-color-scale="white,red"
      feature-properties="RAG_SOC,SCENARIO"
      />
</div>
`
/*


style-function="function (feature) {return {fillColor: '#3388ff'}}"
*/
storiesOf('map.leaflet', module).add('shapefile', () => ({
    components: { Q2ShapefileMap },
    template: template,
    data () {
      return {
        sourceUrl: "https://1.uploads.q-cumber.org/links/6790ef580ab27076847eff1669a8737f832054e95cdd9a419eda8c18/" // null //"/public/NUOVO_PROGETTO_EPSG_32632_OK.zip" // null
      }
    }
  }), {})
