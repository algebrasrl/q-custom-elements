/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue'

import Q2EventsMap from './Q2EventsMap'


const template = `
<div class="contenitore" style="height: 100vh; padding: 0;">
    <Q2EventsMap
      auto
      event-types="qpost_verde,qpost_giallo,qpost_grigio,qpost_rosso,qpost_rosso_ticket,odour_alert"
      />
</div>
`


storiesOf('map.leaflet', module).add('events', () => ({
    components: { Q2EventsMap },
    template: template
  }), {})

