import { latLng } from "leaflet"
import { LMap, LTileLayer, LMarker, LLayerGroup } from "vue2-leaflet"
import 'leaflet.markercluster'

import 'leaflet/dist/leaflet.css'
import 'leaflet.markercluster/dist/MarkerCluster.css'
import 'leaflet.markercluster/dist/MarkerCluster.Default.css'

export default {
  components: {
    LMap,
    LTileLayer,
    LMarker,
    LLayerGroup
  },
  props: {
    mapZoom: {
      type: Number,
      default: 6
    },
    mapCenter: {
      type: Object,
      default () {
        return latLng(41.890251, 12.492373)
      }
    },
    mapDebounceTimeout: {
      type: Number,
      default: 499
    }
  },
  data() {
    return {
      //tileServerUrl: 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
      //attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors',
      tileServerUrl: 'https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}',
      attribution: 'Tiles &copy; Esri',
      
      mapOptions: {
        zoomSnap: 0.5,
        minZoom: 2,
        zoomControl: false,
      },

      cluster: null,

      showMap: true,

      previousMapCenter: this.mapCenter,

      previousMapBounds: null,

      previousMapZoom: this.mapZoom,
      mapZoomingIn: false,
      mapZoomingOut: false,
      flyToZoom: 15,

      selectedMarkerId: null,
      selectedMarkerLat: null,
      selectedMarkerLon: null,

      mapUpdating: true,
      clusterOpened: false
    }
  },
  methods: {
    initUpdate () {
      this.$refs.map.mapObject.on('zoomstart', () => {
        this.$emit('map-moving')
      }).on('movestart', () => {
        this.$emit('map-moving')
      }).on('dragstart', () => {
        this.$emit('map-moving')
      }).on('zoomend', () => {
        this.$emit('map-stop-moving')
      }).on('moveend', () => {
        this.$emit('map-stop-moving')
      }).on('dragend', () => {
        this.$emit('map-stop-moving')
      })

      this.cluster = L.markerClusterGroup({
        showCoverageOnHover: false,
	      zoomToBoundsOnClick: false,
        spiderfyOnMaxZoom: true,
        animate: false
      })
      this.cluster.on('clusterclick', (a) => {
        this.$emit('map-moving')
        a.layer.zoomToBounds({padding: [20, 20]})
      }).on('animationend', (a) => {
        this.$emit('map-stop-moving')
      }).on('spiderfied', (a) => {
        this.$emit('map-moving')
      }).on('unspiderfied', (a) => {
        this.$emit('map-stop-moving')
      })
      this.$refs.map.mapObject.addLayer(this.cluster)

      //this.centerUpdate(this.$refs.map.mapObject.getCenter())
      this.boundsUpdate()
      this.mapUpdating = false
    },
    pointWithinViewport (lat, lon) {
      // TODO
      let latDistance = Math.abs(lat - this.previousMapCenter.lat)
      let lonDistance = Math.abs(lon - this.previousMapCenter.lng)  
      let minDistance = 0.0005
      let underMinDistance = (latDistance < minDistance && lonDistance < minDistance)
      console.log('pointWithinViewport', latDistance, lonDistance, minDistance, underMinDistance)
      return underMinDistance
    },
    zoomUpdate(z) {
      if (z != this.previousMapZoom) {
        console.log('zoomUpdate', z)
        if (z > this.previousMapZoom) {
          this.mapZoomingIn = true
          this.mapZoomingOut = false
        } else {
          this.mapZoomingIn = false
          this.mapZoomingOut = (z !== this.previousMapZoom)
        }
        this.previousMapZoom = z

        this.$emit('zoom-updated')
      }
    },
    centerUpdate(center) {
      if (!this.pointWithinViewport(center.lat, center.lng)) {
        console.log('centerUpdate')
        
        this.boundsUpdate()

        this.previousMapCenter = JSON.parse(JSON.stringify(center))
        this.$emit('center-updated')

      } else {
        console.log('center not changed')
      }
    },
    boundsUpdate() {
      if (this.previousMapBounds) {
        let mapBounds = this.$refs.map.mapObject.getBounds()

        if (mapBounds._northEast.lat != this.previousMapBounds._northEast.lat &&
            mapBounds._northEast.lng != this.previousMapBounds._northEast.lng &&
            mapBounds._southWest.lat != this.previousMapBounds._southWest.lat &&
            mapBounds._southWest.lng != this.previousMapBounds._southWest.lng) {
              this.previousMapBounds = JSON.parse(JSON.stringify(mapBounds))
              this.$emit('bounds-updated')
        } else {
          console.log('bounds not changed')
        }
      } else {
        this.previousMapBounds = JSON.parse(JSON.stringify(this.$refs.map.mapObject.getBounds()))
        this.$emit('bounds-updated')
      }
    },
    flyTo(lat, lon, zoom) {
      if (zoom == undefined) {
        console.log('zoom undefined')
        zoom = this.flyToZoom
      }
      //this.$refs.map.setCenter(latLng(lat, lon))
      this.$refs.map.mapObject.flyTo(latLng(lat, lon), zoom, {
        animate: true,
        duration: 1
      })
    },
    flyToSelected () {
      console.log('flyToSelected')
      let latPrecision = (this.selectedMarkerLat + "").split(".")[1].length
      let lonPrecision = (this.selectedMarkerLon + "").split(".")[1].length
      let center = this.$refs.map.mapObject.getCenter()
      let cLat = Number((center.lat).toFixed(latPrecision))
      let cLon = Number((center.lng).toFixed(lonPrecision))

      if (cLat != this.selectedMarkerLat && cLon != this.selectedMarkerLon) {
        console.log('changing', cLat, cLon, this.selectedMarkerLat, this.selectedMarkerLon, latPrecision, lonPrecision, this.previousMapZoom)

        let zoom = this.flyToZoom
        if (this.previousMapZoom > this.flyToZoom) {
          zoom = this.previousMapZoom
        }
        this.flyTo(this.selectedMarkerLat, this.selectedMarkerLon, zoom)
        
      } else {
        console.log('keeping', cLat, cLon, this.selectedMarkerLat, this.selectedMarkerLon, latPrecision, lonPrecision)
      }

      this.openSelectedMarkerPopup()

    },
    openSelectedMarkerPopup () {
      if (this.selectedMarkerId) {
        console.log('check selecting marker openSelectedMarkerPopup')

        let selectedFixture = this.fixtures.filter((el) => {
          return el.uuid == this.selectedMarkerId
        })
        if (selectedFixture.length) {
          console.log('selectedFixture', selectedFixture[0])
          selectedFixture[0].marker.layer.openPopup()
        }
        /*
        let markers = this.$refs.map.mapObject._layers
        let selectedMarker = null
        for (let m_id in markers) {
          console.log('m', m_id, markers[m_id])
        }
        console.log('selectedMarker', selectedMarker)
        */
      }
    }
  }
}