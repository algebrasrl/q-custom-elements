MdcDialog

## Examples

Default:

```jsx
<div>

<MdcDialog
  ref="dialog"
  >
  <h2>
    Lorem Ipsum
  </h2>
  <p>{{loremIpsum}}</p>
</MdcDialog>

<button @click="$refs.dialog.open()">Show dialog</button>

</div>
```

Actions:

```jsx
<div>

<MdcDialog
  ref="dialog"
  show-actions
  show-close-button
  >
  <h2>
    Lorem Ipsum
  </h2>
  <p>{{loremIpsum}}</p>

  <button slot="actions" type="button" class="mdc-dialog__button" data-mdc-dialog-action="close">
    <span class="mdc-button__label">Close</span>
  </button>
</MdcDialog>

<button @click="$refs.dialog.open()">Show dialog</button>

</div>
```
