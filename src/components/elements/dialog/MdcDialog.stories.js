/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue'

import MdcDialog from './MdcDialog'
import notes from './MdcDialog.md'


const template = `
<div class="contenitore">
<MdcDialog
  ref="dialog"
  >
  <h2>
    Lorem Ipsum
  </h2>
  <p>loremIpsum</p>
</MdcDialog>

<button @click="$refs.dialog.open()">Show dialog</button>
</div>
`

storiesOf('dialog', module)
  .add('MdcDialog', () => ({
    components: { MdcDialog },
    template: template
  }), {
    notes
  })
