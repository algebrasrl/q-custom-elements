ToolsButton

## Examples

### Default

```jsx
<ToolsButton>
  Text
</ToolsButton>

```

### Fill width

```jsx
<ToolsButton
  fill-width
>
  Text
</ToolsButton>

```

### Right aliged

```jsx
<ToolsButton
  align-right
>
  Text
</ToolsButton>

```

### Half width (right aligned)

```jsx
<ToolsButton
  align-right
  half-width
>
  Text
</ToolsButton>

```
