/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue'

import ToolsButton from './ToolsButton'
import notes from './ToolsButton.md'


const template = `
<div class="contenitore tools" style="height: 120px;">
    <ToolsButton>
      Click
    </ToolsButton>
</div>
`

storiesOf('tools', module)
  .add('ToolsButton', () => ({
    components: { ToolsButton },
    template: template
  }), {
    notes
  })
