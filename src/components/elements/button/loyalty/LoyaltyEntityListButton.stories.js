/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue'

import LoyaltyEntityListButton from './LoyaltyEntityListButton'
import notes from './LoyaltyEntityListButton.md'


const template = `
<div class="contenitore loyalty">
    <LoyaltyEntityListButton
        left>
      Click
    </LoyaltyEntityListButton>
    <LoyaltyEntityListButton
      right>
      Click
    </LoyaltyEntityListButton>
</div>
`

storiesOf('loyalty', module)
  .add('LoyaltyEntityListButton', () => ({
    components: { LoyaltyEntityListButton },
    template: template
  }), {
    notes
  })
