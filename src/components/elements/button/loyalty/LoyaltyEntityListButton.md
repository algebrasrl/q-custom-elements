LoyaltyEntityListButton

## Examples

### Default

```jsx
<LoyaltyEntityListButton>
  Click
</LoyaltyEntityListButton>
```

### Right

```jsx
<LoyaltyEntityListButton
  right>
  Click
</LoyaltyEntityListButton>
```

