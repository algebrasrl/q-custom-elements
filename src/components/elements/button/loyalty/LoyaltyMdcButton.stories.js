/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue'

import LoyaltyMdcButton from './LoyaltyMdcButton'
import notes from './LoyaltyMdcButton.md'


const template = `
<div class="contenitore white">

    <LoyaltyMdcButton>
      Click
    </LoyaltyMdcButton>

    <div style="background-color: #111;">
        <LoyaltyMdcButton
          dark
          >
          Click
        </LoyaltyMdcButton>
    </div>

</div>
`

storiesOf('loyalty', module)
  .add('LoyaltyMdcButton', () => ({
    components: { LoyaltyMdcButton },
    template: template
  }), {
    notes
  })