LoyaltyMdcButton

## Examples

### Default

```jsx
<LoyaltyMdcButton>
  Text
</LoyaltyMdcButton>
```

### Dark

```jsx
<LoyaltyMdcButton
  dark>
  Text
</LoyaltyMdcButton>
```
