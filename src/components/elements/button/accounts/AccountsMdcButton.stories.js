/* eslint-disable import/no-extraneous-dependencies */
import { storiesOf } from '@storybook/vue'

import AccountsMdcButton from './AccountsMdcButton'
import notes from './AccountsMdcButton.md'


const template = `
<div class="contenitore">
    <AccountsMdcButton>Click</AccountsMdcButton>
    <br/><br/>
    <AccountsMdcButton
      inverted
      >Click</AccountsMdcButton>
    <br/><br/>
    <AccountsMdcButton
      red
      >Click</AccountsMdcButton>
</div>
`

storiesOf('accounts', module)
  .add('AccountsMdcButton', () => ({
    components: { AccountsMdcButton },
    template: template
  }), {
    notes
  })
