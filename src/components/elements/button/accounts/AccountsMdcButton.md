AccountsMdcButton

## Examples

### Default

```jsx
<div style="position: relative; overflow: hidden; padding: 16px;">
  <AccountsMdcButton>
    Text
  </AccountsMdcButton>
</div>
```

### Inverted

```jsx
<div style="position: relative; overflow: hidden; padding: 16px;">
  <AccountsMdcButton
    inverted
    >
    Text
  </AccountsMdcButton>
</div>
```

### Custom colors

```jsx
<div style="position: relative; overflow: hidden; padding: 16px; background-color: #00bea4;">
  <AccountsMdcButton
    color="#111"
    background-color="#fff"
    >
    Text
  </AccountsMdcButton>
</div>
```

### Big

```jsx
<div style="position: relative; overflow: hidden; padding: 16px;">
  <AccountsMdcButton
    big
    >
    Text
  </AccountsMdcButton>
</div>
```

### Fill

```jsx
<div style="position: relative; overflow: hidden; padding: 16px; height: 64px;">
  <AccountsMdcButton
    fill
    >
    Text
  </AccountsMdcButton>
</div>
```
