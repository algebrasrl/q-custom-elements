import Vue from 'vue'
import axios from 'axios'

import moment from 'moment'
//import 'moment/locale/it'

import HighchartsVue from 'highcharts-vue'
// import VueCustomElement from 'vue-custom-element'
import VueGoogleMaps from '@paiuolo/pai-vue-google-maps/plugins/VueGoogleMaps'
import VueGettext from '@paiuolo/pai-vue-gettext/plugins/VueGettext'


// Configurazione istanza Vue
axios.defaults.xsrfCookieName = 'csrftoken'
axios.defaults.xsrfHeaderName = 'X-CSRFToken'

Vue.prototype.$http = axios
Vue.config.productionTip = false

Vue.use(VueGoogleMaps, {
  apiKey: 'AIzaSyDL1H11jv4GrBCn4L8eVmhE2ZomxiFtqJU',
  libraries: 'places'
})

// moment.locale('it')
Vue.use(VueGettext, {
  callback: function (lang) {
    moment.locale(lang)
  }
})

// Vue.use(VueCustomElement)
Vue.use(HighchartsVue)


// // Test Mocking
// import MockAdapter from 'axios-mock-adapter'
//
// var mock = new MockAdapter(axios)
//
// // mock.onAny().reply(config => {
// //   console.log('REQ', config.method, config.url)
// // })
//
// mock.onGet('https://accounts.q-cumber.org/api/v1/auth/user/').reply(200,
//   {"id":"9cfd1300-c3c6-4f7b-a2f8-c65a4edec917","date_joined":"2018-10-02T21:47:49.055122Z","rev":35,"username":"pai","is_unsubscribed":false,"is_active":true,"subscriptions":[],"email_verified":true,"email":"paiuolo@gmail.com","role":null,"first_name":"Luca","last_name":"Bertuol","description":"pop","picture":"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mP8xso6GQAEjgGVIgvflQAAAABJRU5ErkJggg==","birthdate":null,"latitude":12.565679,"longitude":104.990963,"country":"KH","address":"Cambogia","language":"it"}
// )
//
// mock.onGet('https://elasticsearch.q-cumber.org/').reply(200,
//   {"took":7,"timed_out":false,"_shards":{"total":1,"successful":1,"skipped":0,"failed":0},"hits":{"total":79,"max_score":0.0,"hits":[]},"aggregations":{"group_by_type":{"doc_count_error_upper_bound":0,"sum_other_doc_count":0,"buckets":[{"key":"qpost_grigio","doc_count":65},{"key":"qpost_verde","doc_count":14}]}}}
// )
//
// mock.onGet('/api/v1/loyalty/campaigns/').reply(200,
//   [{"id":1,"created_at":"2018-10-15T08:34:42.127466Z","name":"Calvisano Eco Fedeltà","slug":"calvisano-eco-fedelta","description":"","logo":"/media/filer_public/a7/a1/a7a174e0-5a71-42ba-8bfd-6b7b99632b2a/logo-comune-calvisano.png","image":"/media/filer_public/eb/77/eb771cf8-1a43-473d-902f-2f09fd302601/eco-calvisano-c.png","url":null,"tos":"<p><strong>Operazione a premi <a href=\"/media/filer_public/fe/e6/fee68c4d-f0ad-4b11-b66c-d717b7983890/def41_2018-10-26_piattaforma-mod_ecoloyalty_calvisano.pdf\" target=\"_blank\">ECOFEDELTA’</a> pe i residenti virtuosi in ordine alla sostenibilità ambientale e sociale.</strong></p>\n\n<p><strong>DESTINATARI</strong></p>\n\n<p>Tutti i cittadini residenti sul territorio di Calvisano che siano:</p>\n\n<ul>\n\t<li>persone fisiche residenti a Calvisano maggiorenni;</li>\n\t<li>persone fisiche titolari di contratti per il servizio di raccolta rifiuti con l’azienda che gestisce il servizio sul territorio di Calvisano (in seguito indicato come “utente”; si considerano le Utenze domestiche);</li>\n\t<li>persone fisiche intestatari di Codice TARI con una situazione di “regolarità” nei pagamenti della TARI.</li>\n</ul>\n\n<p>Restano espressamente esclusi dalla partecipazione all’Operazione a premi tutti coloro i quali non rientrano nelle categorie sopra indicate.</p>\n\n<p><strong>FINALITA’ </strong></p>\n\n<p>La finalità dell’Operazione consiste nel premiare e valorizzare i comportamenti virtuosi dei residenti rispetto alla sostenibilità ambientale e sociale, al fine di rendere le persone consapevoli e protagoniste nel creare comunità sostenibili in modo collaborativo con le Istituzioni, rispetto agli obiettivi di sostenibilità fissati dall’ONU (Sustainable Development Goals – Agenda 2030).</p>\n\n<p>L’aspetto innovativo del progetto è dato dall’impiego di algoritmi di attribuzione dei punteggi basati su misure/modelli validati a livello internazionale.</p>\n\n<p>Istruzioni per trovare l'<a href=\"/media/filer_public/62/45/62453566-1d2d-46ee-a2e8-dbdd5e976e90/info_per_cruscotto_green_card_ex_id_tari_utenti_di_calvisano_1.jpg\" target=\"_blank\">ID TARI</a>.</p>\n\n<p><a href=\"/media/filer_public/fe/e6/fee68c4d-f0ad-4b11-b66c-d717b7983890/def41_2018-10-26_piattaforma-mod_ecoloyalty_calvisano.pdf\" target=\"_blank\">Clicca qui per leggere i criteri e le modalità del progetto Calvisano Eco-Fedeltà</a></p>","slogan":"","campaign_subscribed_message":"<p>Complimenti! Hai aderito alla campagna «Calvisano Eco-Fedeltà».<br>\nOra puoi iniziare a contribuire a migliorare la sostenibilità del tuo territorio e ad acquisire punti di sostenibilità.</p>","campaign_subscription_denied_message":"<p>Gentile Utente,<br>\nper procedere alla definitiva iscrizione al portale, sono necessarie delle verifiche amministrative legate al Suo codice utente TARI, in quanto risulterebbero alcune anomalie. Per tutti i chiarimenti si può rivolgere:</p>\n\n<ul>\n\t<li>all'Ufficio Tributi e Tariffa Ambientale di CBBO al numero 030/902605;</li>\n\t<li>allo Sportello Ta.Ri. CBBO in Via Cairoli 4 a Calvisano il martedì (8:30-12:00) o il giovedì (10:30-13:30).</li>\n</ul>\n\n<p>Una volta terminate positivamente le verifiche, sarà possibile iscriversi.</p>","external_id_field_name":"ID TARI","subscriber_must_be_in_area":true,"one_subscriber_per_external_id":false,"subscribed":false,"score":0,"score_assignments":[],"score_redemptions":[]}]
// )
// mock.onGet('/api/v1/loyalty/rewards/').reply(200,
//   [{"id":1,"created_at":"2018-10-15T09:34:40.044314Z","name":"Sconto TARI","slug":"sconto-tari","description":"<p>Lo sconto TARI viene applicato sulla base dei criteri definiti dall’Amministrazione Comunale (vd. www.comune.calvisano.bs.it) ed è riservato alle famiglie che hanno accumulato più punti.<br>\nControlla il tuo profilo utente e le tue bollette TARI di acconto e saldo per verificare l’avvenuto raggiungimento della soglia di applicabilità dello sconto.</p>\n\n<p><a target=\"_blank\" href=\"/media/filer_public/4a/a3/4aa3b4c9-2bc0-40d1-9552-55000b91c094/def_261018comportamenti_oggetto_di_valutazione_.pdf\">CRITERI DI VALUTAZIONE</a></p>","logo":"/media/filer_public/a7/a1/a7a174e0-5a71-42ba-8bfd-6b7b99632b2a/logo-comune-calvisano.png","image":"/media/filer_public/eb/77/eb771cf8-1a43-473d-902f-2f09fd302601/eco-calvisano-c.png","campaign":{"id":1,"created_at":"2018-10-15T08:34:42.127466Z","name":"Calvisano Eco Fedeltà","logo":"/media/filer_public/a7/a1/a7a174e0-5a71-42ba-8bfd-6b7b99632b2a/logo-comune-calvisano.png","image":"/media/filer_public/eb/77/eb771cf8-1a43-473d-902f-2f09fd302601/eco-calvisano-c.png","url":null,"subscribed":false},"url":null,"tos":"","slogan":"","is_manually_assigned":true,"score":0,"missing_score":0,"subscription_score":0,"times_obtained":0,"assigned":false}]
// )
//
// // console.log('mock', mock)
// // endMocking


export const VueInstance = Vue;