/**
 * @mixin
 */


export default {
  props: {
    eventsUrl: {
      type: String,
      default: 'https://events.q-cumber.org/public/'
    }
  },
  methods: {
    sendEvent(eventObject) {
      // send single event
      console.log('eventObject', eventObject)

      this.$emit('sendingEvent')
      this.$http.post(this.eventsUrl,
        eventObject,
        {
          withCredentials: true
        }
      ).then((res) => {
        this.$emit('sendEventResponse', res.data)
      }).catch((err) => {
        //console.log('Error sending event', err)
        this.$emit('sendEventError', 'Error sending events.')
      })
    },
    sendEventP (eventObject) {
      // send single event _(async)
      return new Promise((resolve, reject) => {
        this.$on('sendEventResponse', (response) => {
          resolve(response)
        })
        this.$on('sendEventError', (error) => {
          reject(error)
        })

        this.sendEvent(eventObject)
      })
    },
    async sendEvents (eventObjectsList) {
      // send multiple events (async)
      return Promise.all(eventObjectsList.map(item => this.sendEventP(item)))
    }
  }
}
