/**
 * @mixin
 * Defines the base common interface to interact with
 * piattaforma es serverless functions.
 */

import moment from 'moment'

import LoadingElementMixin from '@paiuolo/pai-vue-mixins/mixins/LoadingElementMixin'
import EntityListMixin from '@paiuolo/pai-vue-mixins/mixins/EntityListMixin'
import xhrGetMixin from '@paiuolo/pai-vue-mixins/mixins/xhrGetMixin'


export default {
  mixins: [
    xhrGetMixin,
    LoadingElementMixin,
    EntityListMixin
  ],
  props: {
    auto: {
      type: Boolean,
      default: true
    },
    sfUrl: {
      type: String,
      default: 'https://data.q-cumber.org/serverless-functions/search-data/'
    },
    size: {  // request elements count
      type: [String, Number],
      default: 10
    },
    from: {
      type: String,
      default: null
    },
    to: {
      type: String,
      default: null
    },
    timezone: {
      type: String,
      default () {
        let offset = (((new Date()).getTimezoneOffset()/60)*-1)
        let sign = (offset >= 0) ? '+' : '-'
        let as_str = String(offset)
        if (as_str.length == 1) as_str = '0' + as_str
        let tz = sign + as_str + ':00'
        //console.log('TIMEZONE', tz, encodeURIComponent(tz))
        return encodeURIComponent(tz)
      }
    },
    useTimezone: {
      type: Boolean,
      default: false
    }
  },
  data () {
    return {
      apiError: null,
      notAuthorized: false,
      xhrGetParams: {
        withCredentials: true
      },
      xhrGetPermittedErrors: '502',
      xhrGetMaxRetries: 5,
      xhrGetRetryTimeout: 1999,
      xhrGetRetryOnPermittedError: true,

      startDate: this.from ? moment(this.from) : null,
      endDate: this.to ? moment(this.to) : null
    }
  },
  mounted () {
    this.$on('performingXhrGet', () => {
      // console.log('performingXhrGet')
      this.startLoading()

      this.$emit('performingGetEntities')
    })
    this.$on('xhrGetResponse', (res) => {
      // console.log('xhrGetResponse', res)
      this.entities = this.parseEntities(res.data)

      this.$emit('getEntitiesSuccess', res.data)
      this.stopLoading()
    })
    this.$on('xhrGetError', (err) => {
      // console.log('xhrGetError')
      if (err.response && err.response.status === 403) {
        this.notAuthorized = true
      } else {
        this.apiError = true
        this.$emit('getEntitiesError', err)
      }
      this.stopLoading()
    })

    if (this.auto) {
      this.getEntities()
    }
  },
  methods: {
    normalizeDate (d) {
      if (this.useTimezone) {
        return d.toString()
      } else {
        return d.toISOString()
      }
    },
    parseEntities (data) {
      let e = data.hits.hits.map((e) => {
        return e['_source']
      })

      return e
    },
    getEntities () {
      this.apiError = false
      this.notAuthorized = false

      this.xhrGetUrl = this.sfUrl

      this.xhrGetUrl += '?size=' + parseInt(this.size)

      if (this.useTimezone) this.xhrGetUrl += '&timezone=' + this.timezone
      if (this.startDate) this.xhrGetUrl += '&from=' + this.normalizeDate(this.startDate)
      if (this.endDate) this.xhrGetUrl += '&to=' + this.normalizeDate(this.endDate)

      this.performXhrGet()
    }
  },
  watch: {
    auto (n) {
      // console.log('auto changed', n)
      if (n) {
        this.getEntities()
      }
    }
  }
}
