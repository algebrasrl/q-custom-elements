import Vue from 'vue'
import VueCustomElement from 'vue-custom-element'

import FriendlyIframe from './components/elements/iframe/FriendlyIframe'


Vue.config.productionTip = false

Vue.use(VueCustomElement)

Vue.customElement('friendly-iframe', FriendlyIframe)

