import Vue from 'vue'
import axios from 'axios'
import moment from 'moment'
// import wordcloudInit from 'highcharts/modules/wordcloud'
// import Highcharts from 'highcharts'
//import HighchartsVue from "highcharts-vue"
import VueCustomElement from 'vue-custom-element'

import VueGettext from '@paiuolo/pai-vue-gettext/plugins/VueGettext'

import Q2ShapefileMap from './components/elements/map/leaflet/shapefile/Q2ShapefileMap'


// wordcloudInit(Highcharts)

Vue.prototype.$http = axios
Vue.config.productionTip = false

// Vue.use(VueCustomElement)
// Vue.use(HighchartsVue)
Vue.use(VueGettext, {
  callback: function (lang) {
    moment.locale(lang)
  }
})

Vue.customElement('q2-shapefile-map', Q2ShapefileMap)
