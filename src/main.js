import { VueInstance as Vue } from './main.requirements.js'

import App from './App.vue'
import router from './router'

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
