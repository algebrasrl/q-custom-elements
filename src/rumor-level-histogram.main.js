import Vue from 'vue'
import axios from 'axios'
import moment from 'moment'

import HighchartsVue from "highcharts-vue"
import VueCustomElement from 'vue-custom-element'

import VueGettext from '@paiuolo/pai-vue-gettext/plugins/VueGettext'

import RumorLevelHistogram from './components/entities/rumor/RumorLevelHistogram/RumorLevelHistogram'


Vue.prototype.$http = axios
Vue.config.productionTip = false

Vue.use(VueCustomElement)
Vue.use(HighchartsVue)
Vue.use(VueGettext, {
  callback: function (lang) {
    moment.locale(lang)
  }
})

Vue.customElement('rumor-level-histogram', RumorLevelHistogram)
