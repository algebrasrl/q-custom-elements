# q-custom-elements

Q-cumber UI elements collection

Lo sviluppo procede definendo dei mixin generici in cui inserire le logiche proprie di ogni
elemento (es. input, textarea, ...).
Tramite la composizione di tali mixin si procede allo sviluppo di componenti più articolati,
definiti in stile SFC (Single File Component) che per facilitare il mantenimento e la compatibilità
cross-browser poggeranno su framework CSS di terze parti (preferentemente material.io).

Per la documentazione, informazioni sull'utilizzo dei componenti e la loro visualizzazione visitare:

https://algebrasrl.bitbucket.io/q-custom-elements/?path=/story/*


## Init

1. Inizializzazione Progetto

    - vue create q-custom-elements
    - cd q-custom-elements
    - vue add router
    - vue add storybook (abilitata la funzionalità "Docs")
    - vue add unit-mocha

2. Inizializzazione delle librerie di supporto

    - npm install -D css-loader sass-loader sass extract-loader file-loader
    - npm i moment axios highcharts swiper
    - npm i vue-slider-component vue-friendly-iframe highcharts-vue
    - npm install @paiuolo/pai-vue-mixins @paiuolo/pai-vue-swiper @paiuolo/pai-vue-google-maps @paiuolo/pai-vue-gettext

3. Inizializzazione delle librerie material components
   (https://material.io/develop/web/docs/getting-started/)

   Verrà installato il pacchetto completo, ogni componente importerà le funzionalità utili

    - npm install material-components-web

4. Configurazione progetto VUE

    - Creato il file main.requirements.js

      Serve a mantenere in un unico file i requirements comuni a tutti i componenti, utile per l'integrazione
      con plugin diversi da vue-cli (storybook, jest, ...)

    - modificato il file main.js

      Contiene le inizializzazioni dell'applicazione Vue

    - Creato il file vue.config.js

      Configura le opzioni di vue-cli

5. Configurazione di Storybook

     - Tolto mdx dalle estensioni abilitate all'autoload in 'config/storybook/main.js'
       (https://github.com/storybookjs/storybook/issues/8096)

     - Aggiunto il file config.js dentro config/storybook/

       Importa requirements di base per tutti i componenti (main.requirements.js) in storybook

     - Aggiunto il file preview-head.html dentro config/storybook/

       Estende html dello storybook

6. Variabili di ambiente

    - Creati i files **.env** e **.env.production** contenenti le variabili di ambiente webpack
      di sviluppo e produzione (url dei backend).

7. Setup ambiente di sviluppo

    Sono stati modificati i files App.vue, router/index.js, Home.vue, HelloWord.vue e adattati a
    "scaffold" per facilitare lo sviluppo dei componenti.


# vue-cli

## Project setup
```
npm install
```

### Compiles and hot-reloads Storybook for development
```
npm run storybook:serve
```

### Compiles Storybook for production

```
# Per il corretto invio dei cookie jwt è necessario aggiungere uk dominio storybook.q-cumber.org ad /etc/hosts 
# 127.0.0.1 storybook.q-cumber.org

npm run storybook:build

# Accedere allo storybook tramite indirizzo:
# https://storybook.q-cumber.org:8080/

```

### Compiles and hot-reloads App.vue for development
```
npm run serve
```

### Compiles and minifies App.vue for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
