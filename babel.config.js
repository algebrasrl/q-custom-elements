module.exports = {
  presets: [
    '@vue/cli-plugin-babel/preset',
  ]
}

/*
module.exports = {
  presets: [
     ['@vue/app', {}],
     [ "@babel/preset-env", {
            useBuiltIns: "usage",
            corejs: {
                version: 3,
                proposals: true
            }
        } ]
  ]
}

module.exports = {
  presets: [
    ['@vue/app', {
      polyfills: [
        'es.promise',
        'es.symbol'
      ]
    }]
  ]
}
*/